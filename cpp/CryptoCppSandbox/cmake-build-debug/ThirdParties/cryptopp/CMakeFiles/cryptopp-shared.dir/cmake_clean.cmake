file(REMOVE_RECURSE
  "libcryptopp.8.6.dylib"
  "libcryptopp.dylib"
  "libcryptopp.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/cryptopp-shared.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
