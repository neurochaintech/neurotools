# CMake generated Testfile for 
# Source directory: /Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/ThirdParties/cryptopp
# Build directory: /Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/cmake-build-debug/ThirdParties/cryptopp
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(build_cryptest "/Applications/CLion.app/Contents/bin/cmake/mac/bin/cmake" "--build" "/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/cmake-build-debug" "--target" "cryptest")
set_tests_properties(build_cryptest PROPERTIES  _BACKTRACE_TRIPLES "/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/ThirdParties/cryptopp/CMakeLists.txt;1218;add_test;/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/ThirdParties/cryptopp/CMakeLists.txt;0;")
add_test(cryptest "/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/cmake-build-debug/ThirdParties/cryptopp/cryptest.exe" "v")
set_tests_properties(cryptest PROPERTIES  DEPENDS "build_cryptest" _BACKTRACE_TRIPLES "/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/ThirdParties/cryptopp/CMakeLists.txt;1219;add_test;/Users/kim/Development/Neurochain/Tools/CryptoSandbox/CryptoCppSandbox/ThirdParties/cryptopp/CMakeLists.txt;0;")
