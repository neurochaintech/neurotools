<style>
h1{
    font-weight: bold;
}
h2{
    font-weight: bold;
}
h3{
    font-weight: bold;
}
table thead tr th {
    font-weight: bold;
    text-align: center;
}
table, th, td {
    border: 1px solid black;
}
alert{
    font-weight: bold;
    color: red;
}
customElement{
    font-weight: bold;
    color: blue;
}
optionElement{
    font-weight: bold;
    color: darkkhaki;
}
</style>

<span style="color: red"></span>

<h1>Change core version</h1>

<h1>Procedure with ansible</h1>

<alert>
    Required: please refer to [create_neurochain_network/ansible](https://gitlab.com/neurochaintech/neurotools/-/tree/main/procedures/create_neurochain_network/ansible) to install NeuroChain network or to check network configuration
</alert>

<h2>Introduction</h2>

<p>
    This procedure helps you:
    <ul>
        <li>change NeuroChain bot with wanted core version</li>
    </ul>
</p>

<h2>Remove core docker from the machine</h2>

<p>
    Log on the wanted host: 
</p>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
</pre>

<p>
    Remove docker container with the image name "registry.gitlab.com/neurochaintech/core/prod/release"
</p>
<pre>
% docker container list
a9d78f55c202 ...

% docker rm a9d78f55c202
</pre>

<h2>Configure ansible</h2>

<p>
    Log on Testnet5-ansible with ssh.
</p>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
</pre>


<alert>
    In ansible/main.yml, comment the line "- import_playbook: 01_blockg.yml" if it is not already the case.
</alert>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% nano ansible/main.yml
</pre>

![01_editMainYml.png](README_imgs/01_editMainYml.png)


<p>
    Launch the installation and add "core_version" with wanted git commit hash, here it is "e2708c6dac14bac6a86b02eda6ecfcd468d1e612". Run the command:
</p>
<pre>
% cd ansible
% ansible-playbook --extra-vars "file_prefix=testnet <alert>core_version=e2708c6dac14bac6a86b02eda6ecfcd468d1e612</alert>" -i testnet/ips.yml main.yml
 </pre>

<h2>Check</h2>

