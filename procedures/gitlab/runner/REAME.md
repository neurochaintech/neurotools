Testnet5-host0		i-0a5ade04a134cdae9	ec2-15-237-124-1.eu-west-3.compute.amazonaws.com	15.237.124.1	–
Testnet5-host1		i-00fe23dce8d4be279	ec2-35-180-67-182.eu-west-3.compute.amazonaws.com	35.180.67.182	–
Testnet5-host2		i-00b453747d40d30fd	ec2-13-37-229-27.eu-west-3.compute.amazonaws.com	13.37.229.27	–
Testnet5-host3		i-0713aa4ff24b540d9	ec2-13-37-230-242.eu-west-3.compute.amazonaws.com	13.37.230.242	–
Testnet5-host4		i-027916184f91399b8	ec2-35-180-125-221.eu-west-3.compute.amazonaws.com	35.180.125.221	–
Testnet5-host5		i-0990a7c8859e8d5d9	ec2-35-181-125-1.eu-west-3.compute.amazonaws.com	35.181.125.1	35.181.125.1
Testnet5-ansible	i-0a4b2481ddc32c37b	ec2-15-188-79-65.eu-west-3.compute.amazonaws.com	15.188.79.65	–

chmod 400 ~/.ssh/neurochain/Testnet5.pem

Testnet5-host0
Gitlab runner
ssh -i ~/.ssh/neurochain/Testnet5.pem ubuntu@ec2-15-237-124-1.eu-west-3.compute.amazonaws.com

Know the architecture
```
dpkg --print-architecture

amd64
```

Download and install binary
```
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# Response
Runtime platform                                    arch=amd64 os=linux pid=13636 revision=0d4137b8 version=15.5.0
```

![addRunner_01](./addRunner_01.png)

Command to register runner
```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN

sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941ybEoPF3kRJ3xygb6dEWD

# Enter a description for the runner:
gitlab-neuro_testnet5-host0
# Enter tags for the runner (comma-separated): 
cpp,dind,docker
# Enter an executor: parallels, virtualbox, docker-ssh+machine, kubernetes, custom, docker, docker-ssh, shell, ssh, docker+machine: 
shell
```

![addRunner_02](./addRunner_02.png)

Update log max size
```
sudo nano /etc/gitlab-runner/config.toml
...
[[runners]]
  ...
  executor = "shell"
  output_limit = 50000
  ...
...

sudo gitlab-runner restart
```

Install requirements for NeuroChain compilation
```
sudo su
passwd gitlab-runner
# set password: gitlab-runner
usermod -aG sudo gitlab-runner

su gitlab-runner
cd ~ 

sudo apt install docker.io
sudo snap install docker

sudo groupadd docker
sudo usermod -aG docker gitlab-runner
newgrp docker
sudo service docker restart

sudo apt-get update -y
sudo apt-get install -y build-essential \
    git \
    cmake \
    libprotobuf-dev \
    protobuf-compiler \
    subversion \
    pkg-config \
    wget \
    tar \
    python3.7 python3-pip python3-setuptools python3-wheel \
    ninja-build \
    mongodb-server \
    libmpfrc++-dev\
    apt-utils \
    software-properties-common \
    rapidjson-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    clang
python3 -m pip install requests
python3 -m pip install gcovr
/usr/bin/ssh-keygen -A

wget http://www.cmake.org/files/v3.22/cmake-3.22.0.tar.gz
tar xf cmake-3.22.0.tar.gz
cd ~/cmake-3.22.0
./configure
sudo make && sudo make install
cd ~
rm cmake-3.22.0.tar.gz

cd ~
git clone -n https://github.com/oktal/pistache.git
cd pistache 
git checkout c5927e1a12b96492198ab85101912d5d84445f67
mkdir build
cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/root/lib/cmake/pistache ..
ninja && ninja install 

exit

sudo gitlab-runner restart
```