# Network map

## Specific

<table>
    <thead>
        <tr>
            <th>Server type</th>
            <th>Name</th>
            <th>IP</th>
            <th>Comments</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Server</td>
            <td>neurochain_gitlab</td>
            <td>192.168.1.33</td>
            <td></td>
        </tr>
        <tr>
            <td>Server</td>
            <td>neurochain_gitlab_runner</td>
            <td>192.168.1.34</td>
            <td></td>
        </tr>
        <tr>
            <td>Server</td>
            <td>neurochain_repository</td>
            <td>192.168.1.37</td>
            <td>private https certificate: myCA</td>
        </tr>
        <tr>
            <td>DNS</td>
            <td></td>
            <td></td>
            <td>local box</td>
        </tr>
        <tr>
            <td>Ansible</td>
            <td>neurochain_alcina</td>
            <td>192.168.1.38</td>
            <td>will deploy files on nodes</td>
        </tr>
        <tr>
            <td>Node</td>
            <td>neurochain_bela</td>
            <td>192.168.1.39</td>
            <td>node 1, Boots + API</td>
        </tr>
        <tr>
            <td>Node</td>
            <td>neurochain_cassandra</td>
            <td>192.168.1.40</td>
            <td>node 2, Chosen</td>
        </tr>
        <tr>
            <td>Node</td>
            <td>neurochain_daniela</td>
            <td>192.168.1.41</td>
            <td>node 3, Bots</td>
        </tr>
    </tbody>
</table>

![local_network_schema](./README_IMGS/local_network_schema.png)

## For all

OS: Ubuntu 18.04 LTS - The Bionic Beaver. Only neurochain_alcina is with a desktop server.

All ".md" with server name are sorted in the install order to create a full tutorial if someone wants to reproduce a local development environment.

<table>
    <thead>
        <tr>
            <th>Server type</th>
            <th>User</th>
            <th>Password</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Server</td>
            <td>root</td>
            <td>toor</td>
        </tr>
        <tr>
            <td>Server</td>
            <td>neurochain</td>
            <td>blockchain</td>
        </tr>
        <tr>
            <td>Node</td>
            <td>root</td>
            <td>no password</td>
        </tr>
        <tr>
            <td>Node</td>
            <td>ubuntu</td>
            <td>ubuntu and connection with .pem</td>
        </tr>
    </tbody>
</table>

To link VM to the local network

![vm_local_network](./README_IMGS/vm_local_network.png)

## To allow ssh root login

```
sudo apt install openssh-server
sudo systemctl status ssh
sudo ufw allow ssh

sudo nano /etc/ssh/sshd_config
...
PermitRootLogin yes
StrictModes yes
...
sudo /etc/init.d/ssh restart
```

![vm_local_network](./README_IMGS/root_ssh.png)

## To share a folder

On VMWare:
```
VM > Settings > Shared Folders
```

On selected server:
```
# If /mnt/hgfs/ doesn't exist :
sudo vmhgfs-fuse .host:/ /mnt/ -o allow_other -o uid=1000

# Check if the shared folder (here vm_shared) is listed:
vmware-hgfsclient

# If the shared folder is listed:
sudo vmhgfs-fuse .host:/vm_shared /mnt/hgfs/ -o allow_other -o uid=1000
```

![vm_share_folder](./README_IMGS/vm_share_folder.png)

## Comments

This tutorial has been deployed with the project NeuroChain core and the branch feature/local_gitlab (commit 0a6424514b397973eb4a1760c179bcdd8e095339) on the 15/02/2022.