# NeuroChain node

Nodes have user ubuntu and can be logged in with .pem file
Root user is without a password.

## Ansible - Alcina

### Set root (superuser) password:
```
sudo -i
passwd
exit
```

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

### Allow ssh root login
```
sudo apt install openssh-server
sudo systemctl status ssh
sudo ufw allow ssh

sudo nano /etc/ssh/sshd_config
...
PermitRootLogin yes
StrictModes yes
...
sudo /etc/init.d/ssh restart
```

### Get ipv4
```
sudo apt install net-tools
ifconfig
```

### SSH key configuration

```
cd ~

# Generating public/private rsa key pair.
ssh-keygen -t rsa -b 2048

# Create a pem file
cd /home/ubuntu/.ssh/
ssh-keygen -f neurochain_network_rsa.pub -m 'PEM' -e > neurochain_network_rsa.pub.pem
```
![ssh_key_gen](./README_IMGS/ssh_key_gen.png)

![ssh_key_gen](./README_IMGS/ssh_key_gen_end.png)

## Node 1 - Bela

### Set root (superuser) password:
```
sudo -i
passwd
exit
```

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

### Allow ssh root login
```
sudo apt install openssh-server
sudo systemctl status ssh
sudo ufw allow ssh

sudo nano /etc/ssh/sshd_config
...
PermitRootLogin yes
StrictModes yes
...
sudo /etc/init.d/ssh restart
```

## Ansible - Alcina

### Spread ssh keys

From neurochain_alcina (192.168.1.38), copy the public key on the remote server (neurochain_bela, 192.168.1.39) which needs to be accessed:
```
cd /home/ubuntu/.ssh/
ssh-copy-id -i /home/ubuntu/.ssh/neurochain_network_rsa.pub root@192.168.1.39
ssh-copy-id -i /home/ubuntu/.ssh/neurochain_network_rsa.pub ubuntu@192.168.1.39
```

![send_ssh_key](./README_IMGS/send_ssh_key.png)

Check it worked:
```
ssh root@192.168.1.39
```
```
ssh -i /home/ubuntu/.ssh/neurochain_network_rsa.pub.pem root@192.168.1.39
```

## Change the computer name (hotname)

Delete the old name and setup new name:
```
sudo nano /etc/hostname
sudo nano /etc/hosts
sudo reboot
```

=> Do the same for neurochain_cassandra (192.168.1.40) and neurochain_daniela (192.168.1.41).