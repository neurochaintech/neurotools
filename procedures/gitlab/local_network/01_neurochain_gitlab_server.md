# NeuroChain Gitlab server

## Install

### Set root password
```
sudo passwd root
```

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

### Install GitLab dependencies
```
sudo apt install curl ca-certificates apt-transport-https gnupg2 -y
```

### Get GitLab references
```
sudo -i
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash
exit
sudo apt update
```

### Install Gitlab
```
sudo apt install gitlab-ce
```

### Configurate GitLab

#### Get IP address
```
ifconfig
...
192.168.1.33
...
```

#### Set Gitlab server URL
```
sudo nano /etc/gitlab/gitlab.rb
# => external_url 'http://192.168.1.33'
sudo gitlab-ctl reconfigure
```

#### Get root password
```
sudo cat /etc/gitlab/initial_root_password
# => 6YUu2XLMWDnG7WTzJeFKisgA0a0PPqS2jytBR2D8ibM=
```

### Access to GitLab
```
http://192.168.1.33
```

## Import project from repository by URL

1. From your GitLab dashboard click New project. 
2. Switch to the Import project tab.
3. Click on the Repo by URL button.
4. Fill in the "Git repository URL" (here https://gitlab.com/neurochaintech/core.git) and the remaining project fields.
5. Click Create project to begin the import process.

![gitlab_import](./README_IMGS/gitlab_import.png)

## Launch gitlab on boot

```
systemctl enable gitlab-runsvdir
```
