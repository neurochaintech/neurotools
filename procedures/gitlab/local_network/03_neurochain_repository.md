# NeuroChain repository

## Install

### Set root password
```
sudo passwd root
```

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

## Configure https

## Create private certificate authority

All below commands below are executed as root.

```
sudo -i
```

### Prepare output directory

```
mkdir ~/certs
cd ~/certs
```

### Create private key

```
openssl genrsa -des3 -out myCA.key 2048

Generating RSA private key, 2048 bit long modulus
.................................................................+++
.....................................+++
e is 65537 (0x10001)
Enter pass phrase for myCA.key:
Verifying - Enter pass phrase for myCA.key: neurochain
```

### Create root certificate

```
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem

Enter pass phrase for myCA.key:
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]: FR
State or Province Name (full name) [Some-State]: Gironde
Locality Name (eg, city) []: Bordeaux
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Home
Organizational Unit Name (eg, section) []:Dev
Common Name (e.g. server FQDN or YOUR name) []:home
Email Address []:info@home
```

## Install root certificate

### Vérifier que le package est installé

```
sudo apt-get install -y ca-certificates
```

### Copy root certificate

```
sudo cp ~/certs/myCA.pem /usr/local/share/ca-certificates/myCA.crt
```

### Update certificats store

```
sudo update-ca-certificates
```

### Test the certificate

```
awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt | grep home

# Results
subject=C = FR, ST = Gironde, L = Bordeaux, O = Home, OU = Dev, CN = home, emailAddress = info@home
```


## Create a certificate for the registry

```
openssl genrsa -out registry.home.key 2048

openssl req -new -key registry.home.key -out registry.home.csr

You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:Gironde
Locality Name (eg, city) []:Bordeaux
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Home
Organizational Unit Name (eg, section) []:Dev
Common Name (e.g. server FQDN or YOUR name) []:Home
Email Address []:info@home

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```


### Create file with extension X509 V3

```
nano registry.home.ext

# Empty first, copy and past:
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = registry.home
```

### Generate a certificate registry.home (DNS name)

```
openssl x509 -req -in registry.home.csr -CA myCA.pem -CAkey myCA.key \
    -CAcreateserial -out registry.home.crt -days 825 -sha256 -extfile registry.home.ext
```

## Docker Registry

### Configure docker

```
mkdir ~/auth

sudo docker run \
    --entrypoint htpasswd \
    httpd:2 -Bbn neurochain neurochain > auth/htpasswd
```

Test on http://192.168.1.37:5000/v2/ with neurochain and neurochain.

#### Deploy docker registry

```
sudo docker run -d -p 5000:5000 --restart=always --name registry \
    -v /root/certs:/certs \
    -v /root/auth:/auth \
    -v /reg:/var/lib/registry \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/registry.home.crt \
    -e REGISTRY_HTTP_TLS_KEY=/certs/registry.home.key \
    -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
    -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
    -e REGISTRY_AUTH=htpasswd registry:2
```

### Test

On your DNS, please add the rule to redirect: registry.home => 192.168.1.37.

```
curl  https://registry.home:5000 
```

```
sudo docker login https://registry.home:5000 -u neurochain -p neurochain

# Results
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```


### Pousser du image de docker de test

```
sudo docker pull ubuntu
sudo docker tag ubuntu registry.home:5000/ubuntu
sudo docker push registry.home:5000/ubuntu
sudo docker image ls

# Results
REPOSITORY                              TAG       IMAGE ID       CREATED       SIZE
ubuntu                                  latest    54c9d81cbb44   2 weeks ago   72.8MB
```

### Send certifate on other nodes
```
# From nodes, install prerequisites 
sudo apt-get install -y ca-certificates

# From the repository server
scp ~/certs/myCA.pem  root@192.168.1.33:/usr/local/share/ca-certificates/myCA.crt
scp ~/certs/myCA.pem  root@192.168.1.34:/usr/local/share/ca-certificates/myCA.crt

scp ~/certs/myCA.pem  root@192.168.1.38:/usr/local/share/ca-certificates/myCA.crt
scp ~/certs/myCA.pem  root@192.168.1.39:/usr/local/share/ca-certificates/myCA.crt
scp ~/certs/myCA.pem  root@192.168.1.40:/usr/local/share/ca-certificates/myCA.crt
scp ~/certs/myCA.pem  root@192.168.1.41:/usr/local/share/ca-certificates/myCA.crt

# From nodes, update certificates
sudo update-ca-certificates
```

If the command "curl https://registry.home:5000" does not work as expected, you might need to reboot your server.