# NeuroChain Gitlab runner

## Install

### Set root password
```
sudo passwd root
```

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

### Using binary file

Download one of the binaries for your system: 
```
# Linux x86-64
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

Give it permissions to execute: 
```
sudo chmod +x /usr/local/bin/gitlab-runner
```

Create a GitLab CI user: 
```
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

Install and run as service: 
```
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
Ensure you have /usr/local/bin/ in $PATH for root or you might get a command not found error. Alternately, you can install gitlab-runner in a different location, like /usr/bin/. 
```
echo $PATH
...:/usr/local/sbin:...
```

### Get ip address for gitlab_server

```
ifconfig
...
192.168.1.34
...
```

## Register a Runner

On the gitlab server (http://192.168.1.33) Get a new token to add a runner: Menu > Admin > Runners > Register an instance runner > Registration token:

![gitlab_tokenForRunner](./README_IMGS/gitlab_tokenForRunner.png)


Back on the runner server (http://192.168.1.34):
```
sudo gitlab-runner register --name gitlab-neuro --url http://192.168.1.33 --registration-token CAU-4ekxxwzFT46gEMuk
# Enter tags for the runner (comma-separated): cpp,dind,docker
# Enter an executor: parallels, virtualbox, docker-ssh+machine, kubernetes, custom, docker, docker-ssh, shell, ssh, docker+machine: shell
```

![gitlab_registerRunner](./README_IMGS/gitlab_registerRunner.png)

![gitlab_registeredRunner](./README_IMGS/gitlab_registeredRunner.png)


### Update log max size

```
sudo nano /etc/gitlab-runner/config.toml
...
[[runners]]
  ...
  executor = "shell"
  output_limit = 50000
  ...
...

sudo gitlab-runner restart
```

![gitlab_runner_logLimit](./README_IMGS/gitlab_runner_logLimit.png)

### Allow http

```
sudo nano /lib/systemd/system/docker.service
...
ExecStart=/usr/bin/docker --insecure-registry myregistry:5000 -d -H fd://
...
sudo systemctl daemon-reload
sudo systemctl restart docker
```

![runner_http](./README_IMGS/runner_http.png)

## Install requirements for NeuroChain compilation

```
sudo su
passwd gitlab-runner
# set password: gitlab-runner
usermod -aG sudo gitlab-runner

su gitlab-runner
cd ~ 

sudo apt install docker.io
sudo snap install docker

sudo groupadd docker
sudo usermod -aG docker gitlab-runner
newgrp docker
sudo service docker restart

sudo apt-get update -y
sudo apt-get install -y build-essential \
    git \
    cmake \
    libprotobuf-dev \
    protobuf-compiler \
    subversion \
    pkg-config \
    wget \
    tar \
    python3.7 python3-pip python3-setuptools python3-wheel \
    ninja-build \
    mongodb-server \
    libmpfrc++-dev\
    apt-utils \
    software-properties-common \
    rapidjson-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    clang
python3 -m pip install requests
python3 -m pip install gcovr
/usr/bin/ssh-keygen -A

wget http://www.cmake.org/files/v3.22/cmake-3.22.0.tar.gz
tar xf cmake-3.22.0.tar.gz
cd ~/cmake-3.22.0
./configure
sudo make && sudo make install
cd ~
rm cmake-3.22.0.tar.gz

cd ~
git clone -n https://github.com/oktal/pistache.git
cd pistache 
git checkout c5927e1a12b96492198ab85101912d5d84445f67
mkdir build
cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/root/lib/cmake/pistache ..
ninja && ninja install 

exit

sudo gitlab-runner restart
```