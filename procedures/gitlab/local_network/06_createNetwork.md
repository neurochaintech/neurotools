# Create NeuroChain network - ansible

What needs to be dne:
- give ansible server ssh access nodes
- give nodes ubuntu users access to sudo wihtout password

## From neurochain_alcina

### Get ansible
```
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible

# The ansible version must be at least >= 2.9.2
ansible --version
```

### Allow passwordless sudo access on all nodes (bela, cassandra and daniela)
```
ssh root@192.168.1.39 (40 then 41)
sudo visudo
...
# Add the line:
ubuntu ALL=(ALL) NOPASSWD:ALL
...
exit
```

![0601_noPassword](./README_IMGS/0601_noPassword.png)

### Prepare tesnet keys
On neurochain_alcina:
```
mkdir ~/keys
cd ansible
mkdir testnet

# Generate /home/ubuntu/keys/testnet.priv:
ssh-keygen
```

![0601_sshKey](./README_IMGS/0601_sshKey.png)


On neurochain_alcina:
```
cat /home/ubuntu/keys/testnet.priv.pub

# Copy the key:
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhK4ODQ1JmotECKcgj8I44tWP7Pngnp8CSziYQLaRrcoij+yLiKuJxOhoXl3kYfYUA7Ftmd5qzXqw7oduCZ0i6LVabu4enUu+sLJlTBQi8jN0Mw4cpAWV6yUJ1yRICoTRYpJfqMcjDp08ChDiR5U0ptqp3MZ78ENPSZNuYhvNfe+NnWGE1Y44F7QLAA5VlMuOrWuWQVPaY/8XqsJE1Tl9eaxjvIgRZBUpBR6Yrqogg761ibhWAKRLiFCuxz6WifFG1F9G71k/cah+QjWsEYKB4E2upFWuLMEaPEra5NKD4bYFP69+5XP6bLxWA2NTWQ1e7L++IQKrSK/l2ACINvqxd ubuntu@ubuntu
```

On all nodes (bela, cassandra and daniela):
```
ssh root@192.168.1.39 (40 then 41)
ssh-keyscan -H 192.168.1.38 >> ~/.ssh/known_hosts
nano .ssh/authorized_keys
# Add copied key
```

![0601_copyKey](./README_IMGS/0601_copyKey.png)

### Setup network
```
# Copy ansible directroy in ~/ 

mkdir  ~/ansible/testnet
cp ~/ansible/public_testnet/ips.yml ~/ansible/testnet/ips.yml
nano ~/ansible/testnet/ips.yml
# Edit the file:
====
all:
  children:
    bots:
      hosts:
        192.168.1.41:
    boots:
      hosts:
        192.168.1.39:
    chosen:
      hosts:
        192.168.1.40:
    api:
      hosts:
        192.168.1.39:
  vars:
    ansible_ssh_user: ubuntu   
    ansible_ssh_private_key_file: /home/ubuntu/.ssh/neurochain_network_rsa
===
```

![0602_ipsYmlConfig](./README_IMGS/0602_ipsYmlConfig.png)

### Launch

If sshpass and python3-pip are not alreday installed on all nodes (bela, cassandra and daniela):
```
ssh root@192.168.1.39 (40 then 41)
sudo apt-get install sshpass
sudo apt-get install python3-pip
pip install docker-py
exit
```

On neurochain_alcina:
```
sudo apt install python-pip
pip install requests
pip install docker
sudo apt install docker.io
sudo chmod 666 /var/run/docker.sock
```

In main.yml, uncomment the line "- import_playbook: 01_blockg.yml" for the first installation, for the creation of the genesis block.
```
cd ~/ansible
cp default_config.json ~/ansible/testnet
nano main.yml
```
![0603_setupMainYml](./README_IMGS/0603_setupMainYml.png)

<!> It is recommented to comment it again once the genesis block has been created.<!>

Finally, to launch the installation, run the command:
```
ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml

OR use this if to give nodes users (here "ubuntu") password (here "ubuntu"):
ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml -kK
```

To install a specific version / commit hash:
```
ansible-playbook -e "file_prefix=testnet core_version=theFullCommitHash" -i testnet/ips.yml main.yml -kK
```

![0604_launch](./README_IMGS/0604_launch.png)

![0605_result](./README_IMGS/0605_result.png)

## Check

Have a look at the blocks creation, connect on the API server (here it is neurochain_bela root@192.168.1.39):
```
ssh root@192.168.1.39
docker exec -it mongo bash
mongo
use testnet
db.blocks.aggregate([{"$group" : {_id:"$branch", count:{$sum:1}}}])

# Results:
{ "_id" : "MAIN", "count" : 13 }
```

Show logs from core
```
docker logs --follow core
```

## To reset

```
cd ~/ansible
ansible-playbook -i testnet/ips.yml ~/ansible/99_clean.yml -kK
ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml -kK
```

## To test on 1 machine only
```
ansible-playbook -i testnet/ips.yml ~/ansible/99_clean.yml -kK --limit 192.168.1.39
ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml -kK --limit 192.168.1.39
```

## To change repository URL

![0606_changeRepoUrl](./README_IMGS/0606_changeRepoUrl.png)

