# Local Gitlab

## Accounts

<table>
    <thead>
        <tr>
            <td>For</td>
            <td>Login</td>
            <td>Password</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>OS</td>
            <td>root</td>
            <td>neurochain</td>
        </tr>
        <tr>
            <td>OS</td>
            <td>neurochain</td>
            <td>toor</td>
        </tr>
        <tr>
            <td>Gitlab</td>
            <td>root</td>
            <td>A9N2p/3MzNUAy7DQEN/2zLampTgdKr2zYZQLWO33xmM=</td>
        </tr>
    </tbody>
</table>

## Install

### Update OS
```
sudo apt update && sudo apt upgrade -y
```

### Install GitLab dependencies
```
sudo apt install curl ca-certificates apt-transport-https gnupg2 -y
```

### Get GitLab references
```
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash
sudo apt update
```

### Install Gitlab
```
sudo apt install gitlab-ce
```

### Configurate GitLab

#### Get IP address
```
ifconfig
...
192.168.1.30
...
```

#### Set Gitlab server URL
```
sudo nano /etc/gitlab/gitlab.rb
# => external_url 'http://192.168.1.30'
sudo gitlab-ctl reconfigure
```

#### Get root password
```
cat /etc/gitlab/initial_root_password
# A9N2p/3MzNUAy7DQEN/2zLampTgdKr2zYZQLWO33xmM=
```

### Access to GitLab
```
http://192.168.1.30
```

## Import project from repository by URL

1. From your GitLab dashboard click New project. 
2. Switch to the Import project tab.
3. Click on the Repo by URL button.
4. Fill in the "Git repository URL" and the remaining project fields.
5. Click Create project to begin the import process.

![gitlab_import](./gitlab_import.png)

## Install GitLab Runners

### Using binary file

Download one of the binaries for your system: 
```
# Linux x86-64
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

Give it permissions to execute: 
```
sudo chmod +x /usr/local/bin/gitlab-runner
```

Create a GitLab CI user: 
```
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

Install and run as service: 
```
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
Ensure you have /usr/local/bin/ in $PATH for root or you might get a command not found error. Alternately, you can install gitlab-runner in a different location, like /usr/bin/. 


Register a Runner

Get a new token to add a runner: Menu > Admin > Runners > Register an instance runner > Registration token
![gitlab_tokenForRunner](./gitlab_tokenForRunner.png)

```
sudo gitlab-runner register --name gitlab-neuro --url http://192.168.1.30 --registration-token DZ2mvzquWKzibpu6RxZE
# Enter tags for the runner (comma-separated): cpp,dind,docker
# Enter an executor: parallels, virtualbox, docker-ssh+machine, kubernetes, custom, docker, docker-ssh, shell, ssh, docker+machine: shell
```

![gitlab_registerRunner](./gitlab_registerRunner.png)

![gitlab_registeredRunner](./gitlab_registeredRunner.png)

If you want to update Gitlab Runner
```
sudo gitlab-runner stop
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner start

nano /etc/gitlab-runner/config.toml
...
output_limit = 50000
...
gitlab-runner restart
```

![gitlab_runner_logLimit](./gitlab_runner_logLimit.png)


## Install Docker

```
# Remove any Docker files that are running in the system
sudo apt-get remove docker docker-engine docker.io

sudo apt install docker.io
sudo snap install docker
docker --version
sudo docker ps -a
```

For error:
```
++ docker login -u gitlab-ci-token -p [MASKED] ''
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.24/auth: dial unix /var/run/docker.sock: connect: permission denied
```
```
sudo groupadd docker
sudo usermod -aG docker gitlab-runner
newgrp docker
service docker restart
```

## Create Docker repository

For error:
```
++ docker login -u gitlab-ci-token -p [MASKED] ''
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Error response from daemon: Get "https://registry-1.docker.io/v2/": unauthorized: incorrect username or password
```
```
cd ~
mkdir auth
docker run \
    --entrypoint htpasswd \
    httpd:2 -Bbn repoUserLogin repoUserPassword > auth/htpasswd

docker run -d \
    -p 5000:5000 \
    --restart=always \
    --name registry \
    -v "$(pwd)"/auth:/auth \
    -e "REGISTRY_AUTH=htpasswd" \
    -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
    -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
    registry:2
```
Test on http://192.168.1.30:5000/v2/ with repoUserLogin and repoUserPassword.

On debian only, set registry to accept http:
```
nano /etc/docker/daemon.json
{
  "insecure-registries" : ["192.168.1.30:5000"]
}
service docker restart
```

Set registry for gitlab
```
nano /etc/gitlab/gitlab.rb
registry_external_url 'http://127.0.0.1'
gitlab_rails['registry_host'] = "127.0.0.1"
gitlab_rails['registry_port'] = "5000"
sudo gitlab-ctl reconfigure
```
![gitlab_setRepo](./gitlab_setRepo.png)


Quick fix: change .gitlab-ci.yml for local usage
```
.build_docker: &build_docker |
    function build_docker() {
        ...
        docker login -u "repoUserLogin" -p "repoUserPassword" "127.0.0.1:5000"
        ...
    }
```
![gitlab_gitlab-ci_yml](./gitlab_gitlab-ci_yml.png)

Fix: core/utils/dockers/Dockerfile.ubuntu
```
FROM ubuntu:18.04

LABEL Description "Dev env image"
ARG DOCKER_BUILD_TYPE=Release

ENV CPPFLAGS="-march=x86-64"
RUN apt-get update -y
RUN adduser  --disabled-password --gecos "" neuro

# Install Packages 
RUN apt-get install -y build-essential git cmake libprotobuf-dev protobuf-compiler subversion pkg-config wget tar\ 
    python3.7 python3-pip python3-setuptools python3-wheel ninja-build\ 
    mongodb-server libmpfrc++-dev\ 
    apt-utils software-properties-common rapidjson-dev libssl-dev libcurl4-openssl-dev\
    clang
RUN python3 -m pip install requests
RUN python3 -m pip install gcovr
RUN /usr/bin/ssh-keygen -A

# Update CMAKE to version 3.22.0
WORKDIR /home/neuro
RUN wget http://www.cmake.org/files/v3.22/cmake-3.22.0.tar.gz
RUN tar xf cmake-3.22.0.tar.gz
WORKDIR /home/neuro/cmake-3.22.0
RUN ./configure
RUN make
RUN make install

RUN g++ -v

WORKDIR /home/neuro
USER root
RUN chown neuro:neuro -R .
RUN chmod 700 -R .

USER neuro
RUN pwd && ls -lahrt && whoami && \
    git clone -n https://github.com/oktal/pistache.git && \
    cd pistache && \
    git checkout c5927e1a12b96492198ab85101912d5d84445f67 && \
    mkdir build && \
    cd build && \
    cmake -G Ninja -DCMAKE_BUILD_TYPE=${DOCKER_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=~/root/lib/cmake/pistache .. && \
    ninja && ninja install 

RUN pwd && ls -lahrt && git clone --single-branch --depth 1 https://gitlab.com/neurochaintech/core.git && \
    mkdir core/build && cd core/build && \
    cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_BUILD_TYPE=${DOCKER_BUILD_TYPE} -G Ninja ..

EXPOSE 22
```

Build image with pipeline CI/CD and then on server:
```
docker image ls
```
![gitlab_buildImageDevDebug](./gitlab_buildImageDevDebug.png)


In core > .gitlab-ci.yml, build_job ask for server dependencies and ressources. So you have to install dependencies on your server for gitlab-runner.
```
sudo mkdir /data
sudo mkdir /data/db

su gitlab-runner
cd ~
sudo apt-get update -y
sudo apt-get install -y build-essential 
sudo apt-get install -y git 
sudo apt-get install -y cmake 
sudo apt-get install -y libprotobuf-dev 
sudo apt-get install -y protobuf-compiler 
sudo apt-get install -y subversion 
sudo apt-get install -y pkg-config 
sudo apt-get install -y wget 
sudo apt-get install -y tar 
sudo apt-get install -y python3.7 python3-pip python3-setuptools python3-wheel 
sudo apt-get install -y ninja-build 
sudo apt-get install -y mongodb-server 
sudo apt-get install -y libmpfrc++-dev
sudo apt-get install -y apt-utils 
sudo apt-get install -y software-properties-common 
sudo apt-get install -y rapidjson-dev 
sudo apt-get install -y libssl-dev 
sudo apt-get install -y libcurl4-openssl-dev
sudo apt-get install -y clang
python3 -m pip install requests
python3 -m pip install gcovr
/usr/bin/ssh-keygen -A

wget http://www.cmake.org/files/v3.22/cmake-3.22.0.tar.gz
tar xf cmake-3.22.0.tar.gz
cd ~/cmake-3.22.0
./configure
make && make install
cd ~
rm cmake-3.22.0.tar.gz

cd ~
git clone -n https://github.com/oktal/pistache.git
cd pistache 
git checkout c5927e1a12b96492198ab85101912d5d84445f67
mkdir build
cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/root/lib/cmake/pistache ..
ninja && ninja install 
```

Restart runner then container 
```
sudo gitlab-ctl stop
docket container list -a
docker start pIDForRegistry:2 
sudo gitlab-ctl start
```