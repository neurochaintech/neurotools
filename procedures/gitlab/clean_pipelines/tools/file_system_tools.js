const fs = require('fs');

class FileSystemTools {
    static doesExist({ inputPath }) {
        var exist = false;
    
        try {
            exist = fs.existsSync(inputPath);
        } catch(err) {
            console.error(err)
        }
    
        return exist;
    }

    static saveToFile({ inputPath, data }) {
        fs.writeFileSync(inputPath, data, function(err) {
            if (err) {
                console.error(err);
            }
        });
    }

    static appendToFile({ inputPath, data }) {
        fs.appendFileSync(inputPath, data + "\n", function(err) {
            if (err) {
                console.error(err);
            }
        });
    }
    
    static readFile({ inputPath }) {
        var content = "";
    
        if( this.doesExist({ inputPath: inputPath }) ) {
            try {
                const rawOutput = fs.readFileSync(inputPath);
                content = rawOutput.toString();
            } catch(err) {
                console.error(err)
            }
        }

        return content
    }

    static readJson({ inputPath }) {
        const content = this.readFile({ inputPath: inputPath });
        return JSON.parse(content);
    }
    
    static saveJsonToFile({ inputPath, data }) {
        fs.writeFileSync(inputPath, JSON.stringify(data, null, 4), function(err) {
            if (err) {
                console.log(err);
            }
        });
    }
    
    static deleteFile({ inputPath }) {
        if(this.doesExist({ inputPath: inputPath })) {
            try {
                fs.unlinkSync(inputPath);
            } catch(error) {
                console.error(error);
            }
        }
    }

    static getFilesPaths({ inputPath, recursive = true, whitelist = ["*"], output = []  }) {
        if ( this.doesExist({ inputPath: inputPath }) ) {
            const files = fs.readdirSync(inputPath);

            for( const file of files ) {
                const path = `${inputPath}/${file}`;

                if( fs.lstatSync(path).isDirectory() ) {
                    this.getFilesPaths({ inputPath: path, recursive: recursive, whitelist: whitelist, output: output} );

                } else {
                    if( whitelist[0] === "*" ) {
                        output.push(path);
                    } else {
                        
                        var add = false;
                        for( const whiteKey of whitelist ) {
                            add |= file.includes(whiteKey);
                        }
                        if(add) {
                            output.push(path);
                        }

                    }
                }
            }
        }

        return output;
    }

    static getDirectoriesPaths({ inputPath, recursive = true, output = []  }) {
        if ( this.doesExist({ inputPath: inputPath }) ) {
            const files = fs.readdirSync(inputPath);

            for( const file of files ) {
                const path = `${inputPath}/${file}`;

                if( fs.lstatSync(path).isDirectory() ) {
                    output.push(path);
                    
                    this.getDirectoriesPaths({ inputPath: path, recursive: recursive, output: output} );
                }
            }
        }

        return output;
    }

    static getNameAndExtension({ inputPath }) {
        var output = "";

        if( this.doesExist({ inputPath: inputPath }) ) {
            try {
                const elements = inputPath.split("/");
                output = elements[ elements.length - 1 ];
            } catch(error) {
                console.error(error);
            }
        }

        return output;
    }

    static deleteDir({ inputPath }) {
        if( this.doesExist({ inputPath: inputPath }) ) {
            fs.rmSync(inputPath, { recursive: true, force: true });
        }
    }

    static createDir({ inputPath }) {
        if( !this.doesExist({ inputPath: inputPath }) ) {
            fs.mkdirSync(inputPath);
        }
    }

    static getStats({ inputPath }) {
        if( this.doesExist({ inputPath: inputPath }) ) {
            return fs.statSync( inputPath )
        }
    }

    static getData({ inputPath }) {
        var data = "";
        
        if( this.doesExist({ inputPath: inputPath }) ) {
            data = fs.readFileSync( inputPath );
        }

        return data;
    }
}

module.exports = FileSystemTools;