const axios = require("axios");

class GitlabApiTools {
    static GITLAB_PROJECT_URL="https://gitlab.com/api/v4";
    static GITLAB_NEUROCHAIN_GROUP_ID=3129967
    static GITLAB_NEUROCHAIN_CORE_PROJECT_ID=8473257
    static PERSONAL_TOKEN=""

    static async getBranches({ page = 0, perPage = 20  }) {
        var data = [];

        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/repository/branches?page=${page}&per_page=${perPage}`;
        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async getBranchCommits({ branchName, page = 0, perPage = 20  }) {
        var data = [];

        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/repository/commits?ref_name=${branchName}&orger=default&page=${page}&per_page=${perPage}`;
        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async getCommit({ sha }) {
        var data;

        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/repository/commits/${sha}`;
        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async getRegistryRepositories({ page = 0, perPage = 20  }) {
        var data = [];

        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/registry/repositories?tags=1&tags_count=true&page=${page}&per_page=${perPage}`;
        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async getRegistryRepositoryTags({ repositoryId, page = 0, perPage = 20  }) {
        var data = [];
        
        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/registry/repositories/${repositoryId}/tags?page=${page}&per_page=${perPage}`;
        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async getPipelines({ page = 0, perPage = 20, whereOptions = "",  orderBy = "", sort = "" }) {
        var data = [];

        var url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/pipelines?page=${page}&per_page=${perPage}`;
        if( whereOptions.length > 0 ) {
            url += `&${whereOptions}`
        }
        if( orderBy.length > 0 ) {
            url += `&order_by=${orderBy}`
        }
        if( sort.length > 0 ) {
            url += `&sort=${sort}`
        }
        console.log(url);

        try {
            const response = ( await axios.get(url) );
            if( response.status === 200) {
                data = response.data;
            }
        } catch(error) {
            console.error(error);
        }

        return data;
    }

    static async deletePipeline({ pipelineId }) {
        const url = `${this.GITLAB_PROJECT_URL}/projects/${this.GITLAB_NEUROCHAIN_CORE_PROJECT_ID}/pipelines/${pipelineId}`;

        try {
            await axios.delete(url,{
                headers: {
                    "PRIVATE-TOKEN": this.PERSONAL_TOKEN
                }
            });
        } catch(error) {
            console.error(error);
        }
    }
}

module.exports = GitlabApiTools;