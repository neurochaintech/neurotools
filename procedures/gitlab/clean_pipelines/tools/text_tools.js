class TextTools {
    static replaceAll({ inputString, elementToFind, replacement }) {
        const regex = new RegExp(elementToFind, 'g');
        return inputString.replace(regex, replacement);
    }

    static isNumeric({ string }) {
        if (typeof string != "string") return false;
        
        return !isNaN(string) &&
               !isNaN(parseFloat(string));
    }
    
    static capitalize({ string }) {
        return string
          .toLowerCase()
          .replace(/\w/, firstLetter => firstLetter.toUpperCase());
    }

    static capitalizeEachWord({ string }) {
        const words = string.split(" ");

        for (let i = 0; i < words.length; i++) {
            words[i] = this.capitalize({ string: words[i] });
        }

        return words.join(" ");
    }
}

module.exports = TextTools;