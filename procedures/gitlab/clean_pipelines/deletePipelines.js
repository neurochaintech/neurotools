const FileSystemTools = require(`./tools/file_system_tools.js`);
const GitlabApiTools = require(`./tools/gitlab_api_tools.js`);
const TextTools = require(`./tools/text_tools.js`);

console.log("=== GITLAB NEUROCHAIN GROUP : " + GitlabApiTools.GITLAB_NEUROCHAIN_GROUP_ID + " ===");

async function main({ deletePipelines = false }) {
    const outputDirPath = `${process.env.PWD}/outputs/`;
    const outputFilePath = `${outputDirPath}/gitlab_clean_summary.csv`;
    FileSystemTools.createDir({ inputPath: outputDirPath });
    FileSystemTools.deleteFile({ inputPath: outputFilePath });
    const header = `Created,Status,Branch,PipelineId,Sha,WebURL`;
    FileSystemTools.appendToFile({ inputPath: outputFilePath, data: `${header}` });

    // const statusArray = ["failed, canceled"];
    for( const status of statusArray) {
        const whereOptions = `status=${status}`;
        const orderBy = `updated_at`;
        const sort = `asc`;
        const pipelines = await GitlabApiTools.getPipelines({ 
            page: 0,
            perPage: Number.MAX_SAFE_INTEGER,
            whereOptions: whereOptions,  
            orderBy: orderBy, 
            sort: sort
        });
        
        console.log(`\n\n=== PIPELINE ${status}: ${pipelines.length} to delete (100 max) ===\n\n`)
        for( const pipeline of pipelines ) {
            const line = `${pipeline.created_at},${pipeline.status},${pipeline.ref},${pipeline.id},${pipeline.sha},${pipeline.web_url}`;
            FileSystemTools.appendToFile({ inputPath: outputFilePath, data: line });

            if(deletePipelines) {
                console.log(`Deleting: ${pipeline.created_at}\t${pipeline.status}\t${pipeline.ref}\t${pipeline.id}\t${pipeline.sha}`);
                await GitlabApiTools.deletePipeline({ pipelineId: pipeline.id });
            }
        }
    }
}

(async () => {
    console.clear();

    await main({ deletePipelines: false });

    process.exit(0);
})();