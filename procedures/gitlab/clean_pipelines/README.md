# GITLAB - How to free space from Usage Quotas

Publishing code on gitlab triggers a process (CI/CD pipeline) that compiles core into a docker container / image.
The latter is saved on a public repository which is called via the installation (ansible).

## Check how much free space is available

Log in and then clock on Gitlab > choose a project > Settings > Usage quotas.

![](README/quotas.png)

## To free space

### Manually: one by one

To perform the solution, you must be at least “Owner” of the project.

In the CI/CD tab of the project > Pipelines > click on one pipeline then on the “Delete" button that appears
.

A core image is currently (24 of february 2022) 2.4 GB.

For information :
* deleting a tag DOES NOT free up space. It us the action of deleting a pipeline that will delete the associated images, tags and other elements.

### Automatically: hundred by hundred

On the gitlab, create an access token: Account > Edit profile > Access Tokens. Choose a name and a deadline, check on "api" and "Create". Note the token value.

![](README/token.png)

```
# Instal prerequisites 
npm run install

# In the script deletePipelines.js, swicth on and off the line (false to get a preview in outputs, true to delete):
await main({ deletePipelines: false });

# Launch scripts
npm run clean
```

This will delete "failed" and "canceled" pipelines using gitlab API. The latter only allow to get 100 pipelines maximum so you need to relaunc N times if you wante to delete more...