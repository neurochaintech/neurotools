ssh -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com
docker exec -t -i core /bin/bash

ls
CMakeFiles           common.grpc.pb.h   config.pb.h           grpcclient           messages.grpc.pb.h     rest.grpc.pb.cc     show_block0
CTestTestfile.cmake  common.pb.cc       consensus.grpc.pb.cc  grpcclient.dir       messages.pb.cc         rest.grpc.pb.h      show_block0.dir
blockg               common.pb.h        consensus.grpc.pb.h   keygen               messages.pb.h          rest.pb.cc          showkey
blockg.dir           conf               consensus.pb.cc       keygen.dir           mongo_update.sh        rest.pb.h           showkey.dir
bot.json             confgen            consensus.pb.h        libcore.a            networking.grpc.pb.cc  root                transaction
cmake_install.cmake  confgen.dir        core                  libprotos.a          networking.grpc.pb.h   service.grpc.pb.cc  transaction.dir
coinbase             config.grpc.pb.cc  core.dir              main                 networking.pb.cc       service.grpc.pb.h   version.h
coinbase.dir         config.grpc.pb.h   data.0.testnet        main.dir             networking.pb.h        service.pb.cc
common.grpc.pb.cc    config.pb.cc       entrypoint.sh         messages.grpc.pb.cc  pistache               service.pb.h

car conf/bot.json
{
    ...
    "random_transaction": 0,
    ...
}





exit

docker exec --interactive core sh -c 'cat ./conf/bot.json'

docker cp core:/home/neuro/config.pb.h ./neurochain_tmp
docker cp core:/home/neuro/config.pb.cc ./neurochain_tmp

exit

scp -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem -r ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com:~/neurochain_tmp/config.pb.h /Users/kim/Development/Neurochain/git/neurotools/shell/outputs/neurochain_core/config.pb.h
scp -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem -r ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com:~/neurochain_tmp/config.pb.cc /Users/kim/Development/Neurochain/git/neurotools/shell/outputs/neurochain_core/config.pb.cc



// Bot

void Bot::handler_heart_beat(const messages::Header &header,
                             const messages::Body &body) {

    if (_config.has_random_transaction() &&
        rand() < _config.random_transaction() * float(RAND_MAX)) {
        send_random_transaction();
    }
}

messages::config::Config _config;

// config.proto

