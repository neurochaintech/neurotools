# Copy keys from bot

## Template

```
scp -o "StrictHostKeyChecking no" -i ${sshKeyPath} ${username}@${hostname}:${remotePath} ${localPath}
```

## Example for Bot-testnet6-1
```
scp -o "StrictHostKeyChecking no" -i ~/.ssh/testnet6_sshKey.pem ubuntu@ec2-13-38-38-5.eu-west-3.compute.amazonaws.com:$~/conf/key.priv ~/BotsKeys/testnet6-1_key.priv
scp -o "StrictHostKeyChecking no" -i ~/.ssh/testnet6_sshKey.pem ubuntu@ec2-13-38-38-5.eu-west-3.compute.amazonaws.com:$~/conf/key.pub ~/BotsKeys/testnet6-1_key.pub

./ECDSA -decipher -privateKeyPath ~/BotsKeys/testnet6-1_key.priv -publicKeyPath ~/BotsKeys/testnet6-1_key.pub
```