<style>
h1{
    font-weight: bold;
}
h2{
    font-weight: bold;
}
h3{
    font-weight: bold;
}
table thead tr th {
    font-weight: bold;
    text-align: center;
}
table, th, td {
    border: 1px solid black;
}
alert{
    font-weight: bold;
    color: red;
}
customElement{
    font-weight: bold;
    color: blue;
}
optionElement{
    font-weight: bold;
    color: darkkhaki;
}
</style>

<span style="color: red"></span>

<h1>Create a NeuroChain network</h1>

<h1>Procedure with ansible</h1>

<h2>Summary</h2>

<h3>Network</h3>

We need machines to host the blockchain. Here is the network configuration:

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Instance ID</th>
            <th>Public IPv4 DNS</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Testnet5-host0</td>
            <td>i-0a5ade04a134cdae9</td>
            <td>ec2-15-237-124-1.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-host1</td>
            <td>i-00fe23dce8d4be279</td>
            <td>ec2-35-180-67-182.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-host2</td>
            <td>i-00b453747d40d30fd</td>
            <td>ec2-13-37-229-27.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-host3</td>
            <td>i-0713aa4ff24b540d9</td>
            <td>ec2-13-37-230-242.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-host4</td>
            <td>i-027916184f91399b8</td>
            <td>ec2-35-180-125-221.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-host5</td>
            <td>i-0990a7c8859e8d5d9</td>
            <td>ec2-35-181-125-1.eu-west-3.compute.amazonaws.com</td>
        </tr>
        <tr>
            <td>Testnet5-ansible</td>
            <td>i-0a4b2481ddc32c37b</td>
            <td>ec2-15-188-79-65.eu-west-3.compute.amazonaws.com</td>
        </tr>
    </tbody>
</table>

<alert>
    In this example, all user accounts are "ubuntu".
</alert>

<h3>Connexion</h3>

They need to keep an address so they can communicate without changing there configuration.
An Elastic IP address is a static IPv4 IP address designed for cloud computing. An Elastic IP address is allocated to an AWS account and is not released until the user action. 

As Testnet5-host5 is the only host with elastic IP, its address won't change. Naturally <b>Testnet5-host5 will host the API</b>. To connect to another machine, we use SSH and .pem file.

<b>Secure Shell (SSH)</b> is both a computer program and a secure communication protocol. The connection protocol requires an exchange of encryption keys at the start of the connection. Subsequently, all TCP segments are authenticated and encrypted. It therefore becomes impossible to use a sniffer to see what the user is doing.

<b>.pem</b> is defined in RFC 1421 to 1424, this is a container format that can include only the public certificate (as with Apache installations and CA certificate files / etc / ssl / certs), or can include a complete certificate chain including a public key, private key, and root certificates. Confusingly, it can also encode a CSR (for example, as used here) because the PKCS10 format can be translated to PEM. The name comes from Privacy Enhanced Mail (PEM), a failed method for secure mail, but the container format used is still valid. This is a base64 translation of the x509 ASN.1 keys.

<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
</pre>

<h3>Permission denied (publickey)</h3>

![00_pemPermissionDenied](README_imgs/00_pemPermissionDenied.png)

<pre>
Permissions 0644 for '<optionElement>localPath/</optionElement/>Testnet5.pem' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "<optionElement>localPath/</optionElement/>Testnet5.pem": bad permissions
ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com: Permission denied (publickey).
</pre>

Solution:

<pre>
% chmod 400 <optionElement>localPath/</optionElement/>Testnet5.pem
</pre>

<h2>Configuration</h2>

<h3>Ansible installation</h3>

<b>Testnet5-ansible</b>

<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% sudo apt update
</pre>
![01_aptUdate.png](README_imgs/01_aptUdate.png)
<pre>
% sudo apt upgrade
</pre>
![02_aptUpgrade_begin.png](README_imgs/02_aptUpgrade_begin.png)
![02_aptUpgrade_end.png](README_imgs/02_aptUpgrade_end.png)
<pre>
% sudo apt install software-properties-common
</pre>
![03_installSoftProp.png](README_imgs/03_installSoftProp.png)
<pre>
% sudo apt-add-repository ppa:ansible/ansible
</pre>
![04_addRepo.png](README_imgs/04_addRepo.png)
<pre>
% sudo apt update
</pre>
![05_aptUdate.png](README_imgs/05_aptUdate.png)
<pre>
% sudo apt install ansible
</pre>
![06_installAnsible.png](README_imgs/06_installAnsible.png)
<pre>
% ansible --version
</pre>
![07_ansibleVersion.png](README_imgs/07_ansibleVersion.png)
<alert>The version must be at least >= 2.9.2</alert>
</pre>

<h3>Ansible scipts configuration</h3>

<b>Copy local ansible.zip file on Testnet5-ansible:</b>

On Testnet5-ansible, prepare directory:
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% mkdir ansible
% ls -al
% exit
</pre>
![08_ansibleDir.png](README_imgs/08_ansibleDir.png)

On the local, send zip to Testnet5-ansible:
<pre>
% scp -i <optionElement>localPath/</optionElement/>Testnet5.pem <optionElement>localPath/</optionElement/>ansible.zip ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com:~/ansible
</pre>

On Testnet5-ansible, unzip:
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% unzip ansible.zip
</pre>

![09_ansibleUnzip.png](README_imgs/09_ansibleUnzip.png)

<b>Give sudo access to other machines</b>

Sudo access on all machines must be passwordless: all IPs and the IP of the machine used for the installation.
Use visudo on all machines so that <customElement>Testnet5-ansible user account</customElement> is allowed to use the sudo command:
<pre>
% <customElement>ubuntu</customElement> ALL=(ALL) NOPASSWD:ALL
</pre>

Example on <b>Testnet5-host0</b>:
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-237-124-1.eu-west-3.compute.amazonaws.com
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit
</pre>

![10_visudo.png](README_imgs/10_visudo.png)

Then repeat this operation on other hosts:
<pre>
For Testnet5-host1:
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-35-180-67-182.eu-west-3.compute.amazonaws.com 
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit

For Testnet5-host2:
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-13-37-229-27.eu-west-3.compute.amazonaws.com 
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit

For Testnet5-host3:
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-13-37-230-242.eu-west-3.compute.amazonaws.com 
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit

For Testnet5-host4:
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-35-180-125-221.eu-west-3.compute.amazonaws.com 
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit

For Testnet5-host5:
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com 
% sudo visudo
% ubuntu ALL=(ALL) NOPASSWD:ALL
% exit
</pre>

<b>Edit ips on Testnet5-ansible</b>

<alert>Generate the ssh keys in "/home/ubuntu/keys/testnet.priv"</alert> for the name of the desired user and <b>copy the <customElement>public key</customElement></b>:
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% mkdir ~/keys
% cd ansible
% mkdir testnet
% ssh-keygen
% cat /home/ubuntu/keys/testnet.priv.pub
<customElement>
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5PtfUIquMqV+FnVhNfMMLbgidmEueOJMDHZacXmMkJ2boZi640m9pUTM4qnMFIUEcDAlE/KFHYccBLUm74F42uL4i4lPuTwOqsYGz3JVBI2ET/ooa9pFItkzYbTS2Jw9G5dZiKdK1qrey1aGPEFgx6nYXlJbA7ZZxIpHVkAbX17NHGNm+kUE16jGsgs++vnT2XdN0LzRRQKcqyfuKhtXWF5rl0S1GSsHwP7jM6Z4ARWewyjtcqA3GGrFhWwsm3MM9jPnizDzNlEvW8gWOBPbJQHHW25La/neo6vkaSvCeTYGE6hJbYHtEHJ9VsBO9hia91qe3H2NEuGwYQvuMeGfv ubuntu@ip-10-0-0-190
</customElement>
</pre>

![11_keyGen.png](README_imgs/11_keyGen.png)
![11_keyCopy.png](README_imgs/11_keyCopy.png)

For all machines, the 6 and localhost, <b>paste the <customElement>public key</customElement></b>:
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-237-124-1.eu-west-3.compute.amazonaws.com
% ssh-keyscan -H ec2-15-188-79-65.eu-west-3.compute.amazonaws.com >> ~/.ssh/known_hosts
% nano .ssh/authorized_keys
</pre>

![11_keyPaste.png](README_imgs/11_keyPaste.png)

Replace IPs with your addresses and [account] with <Testnet5-ansible userAccount>:
<pre>
% nano testnet/ips.yml
 
all:
    children:
        bots:
            hosts:
                <customElement>192.168.0.36</customElement>:
                <customElement>192.168.0.37</customElement>:
                <customElement>192.168.0.38</customElement>:
        boots:
            hosts:
                <customElement>192.168.0.39</customElement>:
                <customElement>192.168.0.40</customElement>:
                <customElement>192.168.0.41</customElement>:
        chosen:
            hosts:
                <customElement>192.168.0.42</customElement>:
        api:
            hosts:
                <customElement>192.168.0.43</customElement>:
    vars:
        ansible_ssh_user: <customElement>user_account</customElement>
        ansible_ssh_private_key_file: /home/<customElement>user_account</customElement>/keys/testnet.priv
</pre>

<b>Boots</b> will be chosen first by a newcomer to obtain the list of active members in the blockchain. When machine starts, it will contact 1 of the 3 boots first to create its list of peers.

<b>Chosen</b> is a member who will host a wallet whose tokens will be distributed to participants, for example to create a faucet in a testnet

<b>Bots</b> is a normal member without special function.

So for this example it becomes:
<pre>
all:
    children:
        bots:
            hosts:
                <customElement>ec2-15-237-124-1.eu-west-3.compute.amazonaws.com</customElement>:
                <customElement>ec2-35-180-67-182.eu-west-3.compute.amazonaws.com</customElement>:
                <customElement>ec2-13-37-229-27.eu-west-3.compute.amazonaws.com</customElement>:
        boots:
            hosts:
                <customElement>ec2-13-37-230-242.eu-west-3.compute.amazonaws.com</customElement>:
                <customElement>ec2-35-180-125-221.eu-west-3.compute.amazonaws.com</customElement>:
                <customElement>ec2-35-181-125-1.eu-west-3.compute.amazonaws.com</customElement>:
        chosen:
            hosts:
                <customElement>ec2-35-180-125-221.eu-west-3.compute.amazonaws.com</customElement>:
        api:
            hosts:
                <customElement>ec2-35-181-125-1.eu-west-3.compute.amazonaws.com</customElement>:
    vars:
        ansible_ssh_user: <customElement>ubuntu</customElement>
        ansible_ssh_private_key_file: /home/<customElement>ubuntu</customElement>/keys/testnet.priv
</pre>

<h2>Launch</h2>

If sshpass and python3-pip are not alreday installed on all machines:
<pre>
% sudo apt-get install sshpass
% sudo apt-get install python3-pip
</pre>

On Testnet5-ansible only:
<pre>
% sudo apt install python-pip
% pip install requests
% pip install docker
% sudo apt install docker.io
% sudo chmod 666 /var/run/docker.sock
</pre>

<b>Testnet5-ansible</b>

Copy Config;
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% cd ansible
% cp default_config.json testnet
</pre>

In main.yml, uncomment the line "- import_playbook: 01_blockg.yml" for the first installation, for the creation of the genesis block. 

<pre>
% nano main.yml
</pre>

![12_mainYml.png](README_imgs/12_mainYml.png)

<alert>
    It is recommented to comment it again once the genesis block has been created.
</alert>

Finally, to launch the installation, run the command: 
<pre>
% ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml
</pre>

![13_ansibleEnd.png](README_imgs/13_ansibleEnd.png)

Have a look at the blocks creation, connect on the <customElement>API server</customElement>, here it is Testnet5-host5 :
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com
% docker exec -it mongo bash
% mongo
% use testnet
% db.blocks.aggregate([{"$group" : {_id:"$branch", count:{$sum:1}}}])
</pre>

![14_mongo.png](README_imgs/14_mongo.png)

<h2>Troubles?</h2>

Some tips to find the problem and the solution:
<ul>
    <li>Check the connected peers: "curl  http://localhost:8080/peers".</li>
    <li>Check the logs: "docker logs --follow core".</li>
    <li>Check the ports on the machine: "sudo lsof -i -P -n | grep LISTEN". 1337 should be open so nodes can communicate. The 8080 should be also open for the API.</li>
    <li>Check the ports on security rules: 1337 and 8080 should be also open.</li>
    <li>Delete or uninstal: "ansible-playbook -e file_prefix=my_new_testnet -i my_new_testnet/ips.yml 99_clean.yml"</li>
</ul>
