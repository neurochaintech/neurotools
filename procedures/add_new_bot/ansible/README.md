<style>
h1{
    font-weight: bold;
}
h2{
    font-weight: bold;
}
h3{
    font-weight: bold;
}
table thead tr th {
    font-weight: bold;
    text-align: center;
}
table, th, td {
    border: 1px solid black;
}
alert{
    font-weight: bold;
    color: red;
}
customElement{
    font-weight: bold;
    color: blue;
}
optionElement{
    font-weight: bold;
    color: darkkhaki;
}
</style>

<span style="color: red"></span>

<h1>Create a new bot and add link it to the NeuroChain network</h1>

<h1>Procedure with ansible</h1>

<alert>
    Required: please refer to [create_neurochain_network/ansible](https://gitlab.com/neurochaintech/neurotools/-/tree/main/procedures/create_neurochain_network/ansible) to install NeuroChain network or to check network configuration
</alert>

<h2>Introduction</h2>

<p>
    This procedure helps you:
    <ul>
        <li>add necessary on a new machine which is NOT linked to NeuroChain network</li>
        <li>install NeuroChain bot</li>
        <li>install NeuroChain database</li>
        <li>link this new machine to the existing NeuroChain network</li>
    </ul>
</p>

<p>
    This will only add a bot, the new machine will not have the "boots", "chosen" or "api" role.
</p>

<h2>Configure ansible</h2>

<p>
    Get ipv4 of the new machine, it can be the DNS machine name or its IPv4. In this example, it will add Testnet5-host2 (ec2-13-37-229-27.eu-west-3.compute.amazonaws.com)
</p>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-13-37-230-242.eu-west-3.compute.amazonaws.com -q -t "hostname -I"

10.0.0.18 172.17.0.1 172.18.0.1 
</pre>


<p>
    Log on Testnet5-ansible with ssh and edit testnet/ips.yml file. Add the DNS machine name or its IPv4 in the wanted role. In this case, it will be a bot.
</p>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% nano ansible/testnet/ips.yml
</pre>

![00_editIps.png](README_imgs/00_editIps.png)

<alert>
    In ansible/main.yml, comment the line "- import_playbook: 01_blockg.yml" if it is not already the case.
</alert>
<pre>
% ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-188-79-65.eu-west-3.compute.amazonaws.com
% nano ansible/main.yml
</pre>

![01_editMainYml.png](README_imgs/01_editMainYml.png)

Launch the installation, run the command: 
<pre>
% cd ansible
% ansible-playbook -e file_prefix=testnet -i testnet/ips.yml main.yml
</pre>

![02_ansibleEnd.png](README_imgs/02_ansibleEnd.png)

<p>
Restarting bots can take a little while. Please wait for 1 minutes for example. If you are worry it did not restrat automatically, you can use [neurochain_docker_admin.sh](https://gitlab.com/neurochaintech/neurotools/-/blob/main/shell/scripts/neurochain_docker_admin.sh) to send restart command to core and database manually.
</p>

![04_restartManually.png](README_imgs/04_restartManually.png)

<h2>Check</h2>

Have a look at the Testnet5-host2's peers and check if some are "CONNECTED":
<pre>
ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-13-37-229-27.eu-west-3.compute.amazonaws.com -q -t "curl -X GET http://10.0.0.18:8080/peers"
</pre>

![03_check.png](README_imgs/03_check.png)

You can also check other the Testnet5-hosts and check if Testnet5-host2 IP (10.0.0.18) is present in their peers configuration. Test on Testnet5-host0:
<pre>
ssh -i <optionElement>localPath/</optionElement/>Testnet5.pem ubuntu@ec2-15-237-124-1.eu-west-3.compute.amazonaws.com -q -t "curl -X GET http://localhost:8080/peers"

...
{
    "endpoint": "10.0.0.18",
    "keyPub": {
       "rawData": "AwqBozpo8e8ZFUcoP1N6gT2mpRlqhFM/mzHTsWsS7GHo"
    },
    "port": 1337,
    "status": "CONNECTED",
    "nextUpdate": {
        "data": 1637000661
    },
    "connectionId": 10
}
...
</pre>