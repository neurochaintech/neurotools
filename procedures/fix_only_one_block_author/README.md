# Fix : blocks are forged by only one author
<style>
h1{
    font-weight: bold;
}
h2{
    font-weight: bold;
}
h3{
    font-weight: bold;
}
table thead tr th {
    font-weight: bold;
    text-align: center;
}
table, th, td {
    border: 1px solid black;
}
alert{
    font-weight: bold;
    color: red;
}
customElement{
    font-weight: bold;
    color: blue;
}
optionElement{
    font-weight: bold;
    color: darkkhaki;
}
</style>

## Symptoms

The chosen host (rf ansible config) is :
- the only one receiving transactions
- the only one host with a bot and a balance greater than 0
- the only blocks author

## Why ?

In ansible process, The script 01_blockg.yml generate N wallets (default 10, keyN.pub avec N [0; 9]) and sets their NeuroChainCoin amount (default 1.000.000.000.000.000).

Wallets keys are saved but never attributed to bots. 
Only the chosen bot is linked to the wallet 0.
To be more precise, it is the only bot that have the same public and private key couple to sign transactions or forge block AND to manage its wallet.

In ansible process, the scipt 06_the_chosen.yml copy past wallet keys for the bot.

To be clear on the usual process :
* bots generate their own keys when they boot and if they don't already have one couple. They use these keys to encrypt data and forge blocks.
* the user creates a wallet and saves the keys in a secure place. Transactions will be created with this key couple.

## How to fix ?

### Retrieve public keys from other bots

<pre>
curl --silent -X GET http://<optionElement>ipv4</optionElement>:8080/status

{
 "bot": {
  ...
  "me": {
   "endpoint": "localhost",
   "keyPub": {
    "rawData": "AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq"
   },
   ...
 }
}
</pre>

###  Retrieve chosen bot private key

<pre>
# Log on the host
% ssh -i <optionElement>localPath/</optionElement>ubuntu@ec2-35-180-125-221.eu-west-3.compute.amazonaws.com // chosen bot

# Log on core docker container
docker exec -t -i core /bin/bash

# Show keys
> ./showkey -k conf/key.priv -p conf/key.pub

public:
{"rawData":"<customElement>AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq</customElement>"}
private:
{"data":"MD4CAQAwEAYHKoZIzj0CAQYFK4EEAAoEJzAlAgEBBCDnN27bsosMZb5SYCePsTfsCCEG8RbEmI1xZTmKflXuhQ=="}
private exponent:
<customElement>e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85h</customElement>
address : 
{"data":"NHWWvB93d8c8xwjGv8rHZkgsJHA1tKGTyY"}
</pre>

To generate transactions, you need to save :
- the public key : <customElement>AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq</customElement>
- the private key : <customElement>e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85</customElement>

<alert>
Please note that the private exponent value is "e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85h", so you need to delete the last character "h" so the string can be considered as an hexadecimal value.
</alert>

### Generate transactions

Generate transactions with the following logic :

* In author wallet, use : 
    - the chosen public key : <customElement>AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq</customElement>
    - the chosen private key : <customElement>e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85</
* In receiver wallet, use : 
    - the other bots public keys : 
        * AvCOGIMLwxr0yoY59HSKVmA4658/BVL+KvSPEogYEkyn
        * AgEwr9cIhv20dboAwlFFfkikXQ0c+K9F7MrEIagDAW1Z
        * ...

Help yourself with the wallet-client in neurotools/js/wallet-client, configure the configurations and launch "npm init-bots".

### Reboot core and bots

You need to reboot core and bots docker containers and wait for some transactions to be generated automatically.
You should see new publick key in block's author field.

