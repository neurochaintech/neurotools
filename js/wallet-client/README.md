# Wallet - Client

## How to install

```
npm install
```

## How to launch

Configure the file ./bin/neurochain_network_hosts.json then
```
npm run init-bots
```

## 2 modes

Two modes are available emplemented :
* development
* production

### Development

This mode launch a server that send automated transactions.
Its purpose is to simulate an active network for NeuroChain.

The main sript scans the network to load dynamically actors.
Actors are hosts with bots that are ready.

Wallet keys are mocked in this case only

```
% npm run dev
% npm run dev-debug
```

### Production (WIP)

This mode waits for the following inputs :
- authorWallet = { public: "...", private: "..." } : author wallet keys
- recipientWallet = { public: "...", private: "..." } : recipient wallet keys
- value : value sent in the transaction in crypto money
- data : data sent in the transaction in raw value (a text for example)
- fee : the transaction fee that will be rewarded to the node that will forge the block

It can also help an user create a wallet pair (public + private key).

In case of the public key loss, a function return the public key from the private key so the user can save it again.

Of course, if the private key is lost, the wallet is unattainable...

```
% npm run prod
```

## Job scheduler 

Launch following commands to launch the server in development every minutes, 1 minute equals 1 transaction:
```
% cd .../wallet-client
% sudo npm install pm2 -g

% pm2 start npm --name "wallet_client" -- dev --cron "*/1 * * * *"

% pm2 list

% pm2 stop wallet_client
```
