const logHost = host => {
    console.log(`## Host`);
    console.log(`Name: ${host.getName()}`);
    console.log(`IP: ${host.getIPv4()}`);
    console.log(`Mac address: ${host.getMacAddress()}`);
}

const logBot = bot => {
    console.log(`## Bot`);
    console.log(`Public key: ${bot.getPublicKey()}`);
    console.log(`Connected peers count: ${bot.getConnectedPeerCount()}`);
}

const logActors = actors => {
    for( const actor of actors ) {
        console.log("# Actor");
        logHost( actor.host );
        logBot( actor.bot );
    }
}

module.exports = { logActors }