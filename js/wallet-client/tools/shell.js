const { execSync } = require("child_process");

const sendCommand = (command, timeout = 1000, silent = false) => {
    var response = "";

    try {
        response = execSync(command, { timeout: timeout }).toString();
    } catch (error) {
        if( !silent ) {
            console.error("SHELL ", error.message);
        }
    }

    return response
}

module.exports = { sendCommand }