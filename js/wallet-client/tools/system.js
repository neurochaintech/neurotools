const fs = require('fs');

const saveToFile = (filePath = "", data) => {
    fs.writeFile(filePath, data, function(err) {
        if (err) {
            console.log(err);
        }
    });
}

const readFile = (filePath = "") => {
    const content = fs.readFileSync(filePath);
    return content.toString();
}

module.exports = { saveToFile, readFile }