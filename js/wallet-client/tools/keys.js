const elliptic = require("elliptic")
const { hexadecimalToBase64, hexadecimalToArrayBase10 } = require("./encoding.js")

const compressKey = keyBasePoint => {
    const x = keyBasePoint.x.toString("hex");

    if (keyBasePoint.y.isEven()) {
        return "02" + x;
    } else {
        return "03" + x;
    }
}

const generateKeys = () => {
    const ecdsa = new elliptic.ec("secp256k1");
    const keyPair = ecdsa.genKeyPair();
    const privateKey = keyPair.getPrivate("hex");
    const publicKey = compressKey(keyPair.getPublic())

    return {
        private: privateKey,
        public: hexadecimalToBase64(publicKey),
    }
}

const getPublicKeyFrom = (privateKey) => {
    const ecdsa = new elliptic.ec("secp256k1");
    const keyPair = ecdsa.keyFromPrivate(privateKey);
    return hexadecimalToBase64(compressKey(keyPair.getPublic()));
}

const hash = (arrayBase10) => {
    const ecdsa = new elliptic.ec("secp256k1");
    return ecdsa.hash().update(arrayBase10).digest("hex");
}

const sign = (key, message) => {
    const ecdsa = new elliptic.ec("secp256k1");
    const ecdsaKey = ecdsa.keyFromPrivate(key, "hex");
    const signature = ecdsaKey.sign(message);
    return signature.r.toString("hex", 64) + signature.s.toString("hex", 64);
}

const signPayload = ( privateKey, payload ) => {
    const arrayBase10 = hexadecimalToArrayBase10(payload);
    const digest = hash(arrayBase10);
    const signature = sign(privateKey, digest);
    return signature
}

module.exports = { generateKeys, signPayload, getPublicKeyFrom }