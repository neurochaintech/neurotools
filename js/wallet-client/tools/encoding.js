
// const isOdd = number => {
//     return number % 2;
// }

// const isEven = number => {
//     return !isOdd(number);
// }

const hexadecimalToArrayBase10 = (hexString, step = 2) => {
    let outputArray = []
    
    for (let i = 0; i < hexString.length; i += step) {
        outputArray.push( parseInt(hexString.substr(i, 2), 16) );
    }

    return outputArray
}

const hexadecimalToBase64 = hexString => {
    return Buffer.from(hexString, 'hex').toString('base64');
}

const base64ToHexadecimal = base64String => {
    return Buffer.from(base64String, 'base64').toString('hex')
}

const binaryToBase64 = binaryString => {
    return Buffer.from(binaryString, 'binary').toString('base64');
}

const binaryToHexadecimal = binaryString => {
    return Buffer.from(binaryString, 'binary').toString('hex');
}

module.exports = { hexadecimalToBase64, base64ToHexadecimal, binaryToBase64, binaryToHexadecimal, hexadecimalToArrayBase10 }