const replaceAll = (inputString = "", elementToFind = "", replacement = "") => {
    const regex = new RegExp(elementToFind, 'g');
    return inputString.replace(regex, replacement);
}

const dateStringYYYYMMDD = (date = Date.now(), joinChar = '') => {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }

    return [year, month, day].join(joinChar);
}

const dateStringYYYYMMDDhhmmss = (date = Date.now(), joinChar = '') => {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear(),
    hour = '' + d.getHours(),
    minutes = '' + d.getMinutes(),
    seconds = '' + d.getSeconds();

    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }
    if (hour.length < 2) {
        hour = '0' + hour;
    }
    if (minutes.length < 2) {
        minutes = '0' + minutes;
    }
    if (seconds.length < 2) {
        seconds = '0' + seconds;
    }

    return [year, month, day, hour, minutes, seconds].join(joinChar);
}

module.exports = { replaceAll, dateStringYYYYMMDD, dateStringYYYYMMDDhhmmss }