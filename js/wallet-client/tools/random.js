const randomInt = (minIncluded, maxIncluded) => {
    return Math.floor(Math.random() * (maxIncluded - minIncluded + 1) + minIncluded)
}

module.exports = { randomInt }