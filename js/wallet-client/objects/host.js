const { sendCommand } = require("../tools/shell.js");
const { replaceAll } = require("../tools/text.js");

module.exports = class Host {
    constructor(inputIPv4, inputName, inputMacAddress) {
        this.loadIPv4(inputIPv4);
        this.loadName(inputName);
        this.loadMacAddress(inputMacAddress)
    }

    loadIPv4(inputIPv4) {
        this.ipv4 = "";

        if( inputIPv4 !== undefined ) {
            this.ipv4 = inputIPv4;
        } else {
            try {
                this.ipv4 = replaceAll(sendCommand("hostname -i"), "\n", "");
            } catch (error) {
                console.error(error);
            }
        }
    }

    loadName(inputName) {
        this.name = "";

        if( inputName !== undefined ) {
            this.name = inputName;
        } else {
            try {
                this.name = replaceAll(sendCommand("hostname -fs"), "\n", "");
            } catch (error) {
                console.error(error);
            }
        }
    }

    loadMacAddress(inputMacAddress) {
        this.macAddress = "";
        
        if( inputMacAddress !== undefined ) {
            this.macAddress = inputMacAddress;
        } else {
            try {
                this.macAddress = replaceAll(sendCommand("cat /sys/class/net/eth0/address"), "\n", "");
            } catch (error) {
                console.error(error);
            }
        }
    }

    getIPv4() {
        return this.ipv4;
    }

    getName() {
        return this.name;
    }

    getMacAddress() {
        return this.macAddress;
    }

    isReady() {
        if( this.getIPv4() === "" ) {
            console.error("ERROR - IP not found.");
            return false;
        }

        if( this.getMacAddress() === ""  ) {
            console.error("ERROR - Mac address not found.");
            return false;
        }

        return true;
    }
};