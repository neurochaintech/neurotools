const fetch = require("node-fetch");
const { sendCommand } = require("../tools/shell.js");

const BOT_API_PROTOCOL = "http";
const BOT_API_PORT = "8080";

module.exports = class Bot {
    constructor( ipv4 = "localhost" ) {
        this.apiURL = `${BOT_API_PROTOCOL}://${ipv4}:${BOT_API_PORT}`;

        return (async () => {
            await this.loadStatus();
            
            return this;
        })();
    }

    async loadStatus() {
        this.status = {};

        try {
            const command = `curl -s -X GET ${this.apiURL}/status`;
            const data = sendCommand(command);
            this.status = JSON.parse(data);
            // console.log(this.status);
        } catch (error) {
            // console.error(error);
        }
    }

    getPublicKey() {
        return this.status.bot.me.keyPub.rawData;
    }

    getConnectedPeerCount() {
        return this.status.peer.connected;
    }

    async isValid() {
        try {
            const command = `curl -s -X POST ${this.apiURL}/validate -d '{ "rawData": "${this.getPublicKey()}"}'`;
            const data = sendCommand(command);
            return data.length === 0;
        } catch (error) {
            console.error(error);
        }

        return false;
    }

    async isReady() {
        try {
            const command = `curl -s -X GET ${this.apiURL}/ready`;
            const data = sendCommand(command, 1000, true);
            return data === "{ok: 1}";
        } catch (error) {
            console.error(error);
        }

        return false;
    }

    async isKeyValid( publicKey ) {
        try {
            const command = `curl -s -X POST ${this.apiURL}/validate -d '{ "rawData": "${publicKey}"}'`;
            const data = sendCommand(command, 1000, true);
            return data.length === 0;
        } catch (error) {
            console.error(error);
        }
    }

    async createTransaction( transaction ) {
        
        try {
            const resp = await fetch(`${this.apiURL}/create_transaction`, {
                method: "POST",
                body: JSON.stringify(transaction),
            })

            console.log("DEBUG - createTransaction, response: \n", resp);

            return resp.text()
        } catch (e) {
            return Promise.reject(e)
        } 
    }

    async publishTransaction( payload, signature, publicKey ) {
        const requestBody = {
            transaction: payload,
            signature: signature,
            keyPub: {rawData: publicKey}
        };

        console.log("DEBUG - publishTransaction, request: \n", requestBody);

        try {
            const resp = await fetch(`${this.apiURL}/publish`, {
                method: "POST",
                body: JSON.stringify(requestBody),
            })
            
            console.log("DEBUG - sendTransaction, response: \n", resp);

            if (!resp.ok) {
                return Promise.reject(await resp.text())
            } else {}
	            return resp.json()
        } catch (e) {
            return Promise.reject(e)
        }
    }
}