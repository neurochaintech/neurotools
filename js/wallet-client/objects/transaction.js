const Bot = require("./bot");
const { randomInt } = require("../tools/random.js");
const { signPayload } = require("../tools/keys.js");
const { dateStringYYYYMMDDhhmmss } = require("../tools/text.js");

module.exports = class Transaction {
    
    constructor( inputAuthorWallet, inputRecipientWallet, inputValue, inputData, inputFee ) {
        this.authorWallet = inputAuthorWallet;
        this.recipientWallet = inputRecipientWallet;
        this.value = inputValue;
        this.data = inputData;
        this.fee = inputFee;

        return (async () => {
            this.bot = await new Bot();
            return this;
        })();
    }

    fillTransaction( authorPublicKey, recipientPublicKey, DEBUG = false ) { 
        const value = this.value !== undefined ? this.value : randomInt(1, 100);
        const fee = this.fee !== undefined ? this.fee : randomInt(1, 5);
        const data = this.data !== undefined ? this.data : `From ${authorPublicKey}, To ${recipientPublicKey}, Sending ${value} NCC with ${fee} NCC of fee, filled at ${dateStringYYYYMMDDhhmmss()}`;

        if(DEBUG) {
            console.log("\nRaw transaction data:", data);
        }
        
        return {
            key_pub: {rawData: authorPublicKey},
            outputs: [
                {
                    key_pub: {rawData: recipientPublicKey},
                    value: {value: value.toString()},
                    data: Buffer.from(data).toString("base64"),
                }
            ],
            fee: fee
        };
    }

    async publish( DEBUG = false ) {
        var keysValid = true;
        keysValid &= await this.bot.isKeyValid( this.authorWallet.public );
        keysValid &= await this.bot.isKeyValid( this.recipientWallet.public );
        
        if( keysValid ) {
            const transaction = this.fillTransaction( this.authorWallet.public, this.recipientWallet.public, DEBUG );
            if(DEBUG) {
                console.log("\nRaw transaction:");
                console.log(JSON.stringify(transaction, null, 4));
                console.log("");
            }

            const payload = await this.bot.createTransaction( transaction );
            if(DEBUG) {
                console.log("Payload:\n" + payload + "\n");
            }
            
            const signature = signPayload( this.authorWallet.private, payload );
            if(DEBUG) {
                console.log("AuthorWallet private key:\n" + this.authorWallet.private + "\n");
                console.log("Signature:\n" + signature + "\n");
            }
            
            // Don't handle rejection to stop the process if there is a problem.
            const returnedTransaction = await this.bot.publishTransaction( payload, signature, this.authorWallet.public )
            console.log("Returned transaction:");
            console.log(returnedTransaction);
        } else {
            console.log("\nERROR - Transaction not filled because keys are invalid");
            console.log("Author: " + this.authorWallet.public);
            console.log("Recipient: " + this.recipientWallet.public);
        }
    }

};