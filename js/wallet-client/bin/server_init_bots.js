const Transaction = require("../objects/transaction.js");
const { sendCommand } = require("../tools/shell.js");
const { saveToFile, readFile } = require("../tools/system.js");

var DEBUG = false;
function setGlobals() {
    process.argv.forEach(function (val, index, array) {
        if( val === "DEBUG" ) {
            DEBUG = true;
        }
    }); 
}

function getWallet(host = {}) {
    return {
        public: host.publicKey,
        // private: host.privateKey,
        private: host.privateExponent
    }
}

async function main() {
    const networkPath = __dirname + "./bin/neurochain_network_hosts.json";
    const networkJson = JSON.parse( readFile(networkPath) );

    var chosenHost = {};
    var hostsToInit = [];
    for( const host of networkJson.hosts ) {
        if( host.role && host.role === "chosen" ) {
            chosenHost = host;
        } else {
            hostsToInit.push(host);
        }
    }

    console.log("Chosen host: ", chosenHost);
    console.log("Hosts / Bots to init: ", hostsToInit.length);


    const authorWallet = getWallet(chosenHost);
    for( const host of hostsToInit ) {
        const recipientWallet = getWallet(host);
        const transaction = await new Transaction( authorWallet, recipientWallet );
        await transaction.publish( DEBUG );
    }
}

(async () => {
    console.log("\n=== Wallet - Development BEGIN ===\n");

    setGlobals();
    await main();

    console.log("\n=== Wallet - Development END ===\n");
})();
