const Host = require("../objects/host.js");
const Bot = require("../objects/bot.js");
const Transaction = require("../objects/transaction.js");
const { sendCommand } = require("../tools/shell.js");
const { saveToFile } = require("../tools/system.js");
const { randomInt } = require("../tools/random.js");
const { generateKeys } = require("../tools/keys.js");
const { logActors } = require("../tools/debug.js");

var DEBUG = false;
function setGlobals() {
    process.argv.forEach(function (val, index, array) {
        if( val === "DEBUG" ) {
            DEBUG = true;
        }
    }); 
}

async function addActor( actors, host ) {

    if( host.isReady() ) {
        const bot = await new Bot( host.getIPv4() );

        if( await bot.isReady() ) {
            actors.push({
                host: host,
                bot: bot
            });
        }
    }
}

async function getActors() {
    var actors = [];

    console.log("Scanning network to find hosts with bots...");
    
    const selfHost = new Host();
    await addActor( actors, selfHost );
    
    const result = sendCommand("arp -a | grep -v 172.18.*"); // -v 172.18. to avoid docker
    const lines = result.split("\n");
    for (var iLine = 0; iLine < lines.length; ++iLine) {
        const array = lines[iLine].split(" ");
        if(array.length > 1) {
            
            const host = new Host(
                array[1].slice(1,-1),
                array[0],
                array[3],
            );
            await addActor( actors, host );
        }
    }

    console.log("\nFound: " + actors.length + " actors.");

    if(DEBUG) {
        logActors(actors)
    }

    return actors;
}

async function loadWalletKeys( actors ) {
    const keysPath =  __dirname + "/../emulation/walletKeys.json";
    var jsonContent = require(keysPath);
    var walletKeys = jsonContent.keys;
    
    var actorAdded = false;
    for( const actor of actors ) {
        const botPublicKey = actor.bot.getPublicKey();
        if( walletKeys[botPublicKey] === undefined ) {

            console.log("Generating keys for: " + botPublicKey);
            var keyPair;
            do {
                keyPair = generateKeys();
                console.log("Try: " + keyPair.public);
            } while( !( await actor.bot.isKeyValid( keyPair.public ) ) )

            walletKeys[botPublicKey] = keyPair;
            actorAdded = true;
        }
    }
    jsonContent.keys = walletKeys;

    if(actorAdded) {
        console.log("Keys updated");
        saveToFile( keysPath, JSON.stringify(jsonContent, null, 4) );
    }

    return walletKeys;
}

async function main() {
    // === For development only - BEGIN ===
    
    const actors = await getActors();
    const walletKeys = await loadWalletKeys( actors );
    
    const iAuthor = randomInt(0, actors.length-1);
    var iRecipient;
    do {
        iRecipient = randomInt(0, actors.length-1);
    } while( iRecipient === iAuthor );

    const author = actors[iAuthor];
    const recipient = actors[iRecipient];
    const authorWallet = walletKeys[ author.bot.getPublicKey() ];
    const recipientWallet = walletKeys[ recipient.bot.getPublicKey() ];

    // === For development only - END ===

    const transaction = await new Transaction( authorWallet, recipientWallet );
    await transaction.publish( DEBUG );

    return 0;
}

(async () => {
    console.log("\n=== Wallet - Development BEGIN ===\n");

    setGlobals();
    await main();

    console.log("\n=== Wallet - Development END ===\n");
})();