const { generateKeys, getPublicKeyFrom } = require("../tools/keys.js");

(async () => {
    console.log("\n=== Wallet - Development BEGIN ===\n");

    const wallet = generateKeys();
    console.log(wallet);

    const publicKey = getPublicKeyFrom(wallet.private);
    console.log(publicKey, wallet.public === publicKey);

    console.log("\n=== Wallet - Development END ===\n");
})();
