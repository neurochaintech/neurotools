{
    "database": {
        "block0File": {
            "blockFormat": "PROTO",
            "blockPath": "conf/data.0.testnet"
        },
        "dbName": "testnet",
        "url": "mongodb://mongo:27017/testnet"
    },
    "logs": {
        "severity": "info",
        "toStdout": true
    },
    "networking": {
        "keysPaths": [
            {
                "keyPrivPath": "conf/key.priv",
                "keyPubPath": "conf/key.pub"
            }
        ],
        "maxConnections": 3,
        "tcp": {
            "endpoint": "localhost",
            "peers": [
