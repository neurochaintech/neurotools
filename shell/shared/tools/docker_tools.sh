dockerTools_listContainers() {
    pem_path=$1
    ipv4=$2

    command="docker container ps -a --no-trunc"    
    
    echo "$( systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}" )"
}

dockerTools_getContainerIdWithImageName() {
    outputId=""

    pem_path=$1
    ipv4=$2
    image_name=$3

    containers_list="$( dockerTools_listContainers ${pem_path} ${ipv4} )"

    IFS=$'\n' read -ra lines -d $'\0' <<< "${containers_list}"
    for line in "${lines[@]}"
    do
        IFS=$' ' read -ra elements -d $'\0' <<< "${line}"
        if [[ ${elements[1]} == *"${image_name}"* ]]; then
            outputId=${elements[0]}
        fi
    done

    echo ${outputId}
}

dockerTools_executeAction() {
    pem_path=$1
    ipv4=$2
    action=$3
    container_id=$4
    
    command="docker ${action} ${container_id}"
    systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}"
}
