systemTools_sendCommand() {
    command=$1
    debug=${2:-false}

    if [ "${debug}" = true ] ; then
        echo $command
    fi

    `${command}`
}

systemTools_sendSshCommand() {
    pem_path=$1
    ipv4=$2
    command=$3
    debug=${4:-false}

    full_command="ssh -i ${pem_path} ubuntu@${ipv4} -q -t  \"${command}\""

    if [ "${debug}" = true ] ; then
        echo $full_command
    fi

    eval ${full_command}
}

systemTools_sendFilesWithSsh() {
    pem_path=$1
    ipv4=$2
    input_path=$3
    output_path=$4
    debug=${5:-false}

    full_command="scp -i ${pem_path} -r ${input_path} ubuntu@${ipv4}:${output_path}"
    
    if [ "${debug}" = true ] ; then
        echo $full_command
    fi

    eval ${full_command}
}

systemTools_getSshIpv4() {
    adress_ip=""

    pem_path=$1
    ipv4=$2
    command="hostname -I"
    adress_ip=$( systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}" )

    if [[ ${adress_ip} == *" "* ]]; then
        IFS=$' ' read -ra elements -d $'\0' <<< "${adress_ip}"
        adress_ip=${elements[0]}
    fi

    echo ${adress_ip}
}