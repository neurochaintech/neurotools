helpTools_userAskedForIt() {
    output=false

    if [ $# -eq 0 ]; then
        output=true
    fi

    if [ "$1" == "-h" ]; then
        output=true
    fi

    echo "${output}"
}