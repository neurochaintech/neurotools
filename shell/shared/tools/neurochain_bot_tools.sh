neurochainBotTools_getStatus() {
    pem_path=$1
    ipv4=$2
    ip_address=$3
    bot_api_port=$4

    command="curl -X GET http://${ip_address}:${bot_api_port}/status"
    echo "$( systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}" )"
}

neurochainBotTools_getPublicKey() {
    pem_path=$1
    ipv4=$2
    ip_address=$3
    bot_api_port=$4

    command="curl -X GET http://${ip_address}:${bot_api_port}/status -s | jq -r .bot.me.keyPub.rawData | tr -d '\n'"
    echo "$( systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}" )"
}