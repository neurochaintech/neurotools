#!/bin/bash

clear

source "../shared/tools/system_tools.sh"

pem_path=/Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem
host_ipv4dns=ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com

file_name=config.pb.h

# TODO

# Export from docker to host
# command_export_from_docker="docker cp core:/home/neuro/${file_name} ./neurochain_tmp"
# systemTools_sendSshCommand "${pem_path}" "${host_ipv4dns}" "${command_export_from_docker}"

# Export from host to local
# current_date=$(date '+%Y%m%d')
# mkdir -p "../outputs/neurochain_core"
# output_path="../outputs/neurochain_core/${file_name}"
# if [ -f "${output_path}" ]; then
#     echo "" > ${output_path}
# fi
# scp -i "${pem_path}" -r "${host_ipv4dns}":~/neurochain_tmp/${file_name} ${output_path}

# echo "${file_name} retrieved in ${output_path}"