#!/bin/bash

clear

source "config.sh"
source "../shared/tools/log_tools.sh"
source "../shared/tools/docker_tools.sh"
source "../shared/tools/system_tools.sh"

showKeysOnHost() {
    host_name=$1
    host_ipv4dns=$2

    # command="docker exec -it core './showkey -h'"
    command="docker exec --interactive --tty core /bin/bash -c './showkey -k conf/key.priv -p conf/key.pub'"
    keys_raw=$( systemTools_sendSshCommand "${PEM_PATH}" "${host_ipv4dns}" "${command}" "true" )
    echo "${keys_raw}"
    # keys=$( head -4 "${keys_raw}" )
}


for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        showKeysOnHost "${NETWORK_HOSTS_NAMES[i_host]}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}"
        printf "\n\n\n"
done
