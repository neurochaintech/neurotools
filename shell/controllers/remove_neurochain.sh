########
# Main #
########

clear

source "config.sh"
source "../shared/tools/log_tools.sh"
source "../shared/tools/docker_tools.sh"
source "../shared/tools/system_tools.sh"

removeOnHost() {
    host_name=$1
    host_ipv4dns=$2

    # Removing docker containers

    printf "# Cleaning ${host_name} (${host_ipv4dns})\n\n"

    printf "## Removing docker containers\n"

    core_id=$( dockerTools_getContainerIdWithImageName "$PEM_PATH" "${host_ipv4dns}" "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}" )
    if [[ ! -z "${core_id}" ]]
    then
        dockerTools_executeAction "$PEM_PATH" "${host_ipv4dns}" "stop" "${core_id}"
        dockerTools_executeAction "$PEM_PATH" "${host_ipv4dns}" "rm" "${core_id}"
    else
        echo "Container already removed: ${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}"
    fi

    database_id=$( dockerTools_getContainerIdWithImageName "$PEM_PATH" "${host_ipv4dns}" "${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}" )
    if [[ ! -z "${database_id}" ]]
    then
        dockerTools_executeAction "$PEM_PATH" "${host_ipv4dns}" "stop" "${database_id}"
        dockerTools_executeAction "$PEM_PATH" "${host_ipv4dns}" "rm" "${database_id}"
    else
        echo "Container already removed: ${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}"
    fi

    # Removing linked files and directories

    printf "## Removing conf directory\n"
    conf_path="/home/ubuntu/conf"
    file_count=$( systemTools_sendSshCommand "${PEM_PATH}" "${host_ipv4dns}" "ls ${conf_path} | wc -l" )
    if [ "${file_count}" > "0" ]; then 
        command="rm -rf ${conf_path}"
        systemTools_sendSshCommand "${PEM_PATH}" "${host_ipv4dns}" "${command}"
    else 
        echo "Directory already removed: ${conf_path}"
    fi

    printf "## Removing mongo directory\n"
    mongo_path="/home/ubuntu/mongo"
    file_count=$( systemTools_sendSshCommand "${PEM_PATH}" "${host_ipv4dns}" "ls ${mongo_path} | wc -l" )
    if [ "${file_count}" > "0" ]; then 
        command="rm -rf ${mongo_path}"
        systemTools_sendSshCommand "${PEM_PATH}" "${host_ipv4dns}" "${command}"
    else 
        echo "Directory already removed: ${mongo_path}"
    fi
}


# removeOnHost "Testnet5-host3" "ec2-13-37-230-242.eu-west-3.compute.amazonaws.com"
for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        removeOnHost "${NETWORK_HOSTS_NAMES[i_host]}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}"
        printf "\n\n\n"
done
