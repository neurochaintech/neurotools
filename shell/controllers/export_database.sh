getCollectionNames() {
    pem_path=$1
    ipv4=$2
    database_name=$3

    command="docker exec -i mongo mongo ${database_name} --quiet --eval 'db.getCollectionNames()'"
    collections_mongo=$( systemTools_sendSshCommand "${pem_path}" "${ipv4}" "${command}" )

    collections_clean_string=$( echo "${collections_mongo//[[\]\"]}" )
    echo "${collections_clean_string}"
}

exportCollectionAndDownload() (
    pem_path=$1
    ipv4=$2
    database_name=$3
    collection_name=$4

    echo "Export collection ${collection_name} from mongo to docker"
    command_export_from_mongo="docker exec -i mongo mongoexport --db ${database_name} --collection ${collection_name} --out ./${collection_name}.json"
    systemTools_sendSshCommand "${pem_path}" "${host_ipv4dns}" "${command_export_from_mongo}"

    echo "Export collection ${collection_name} from docker to host"
    command_prepare_output="mkdir -p ~/neurochain_db"
    systemTools_sendSshCommand "${pem_path}" "${host_ipv4dns}" "${command_prepare_output}"

    command_export_from_docker="docker cp mongo:/${collection_name}.json ./neurochain_db"
    systemTools_sendSshCommand "${pem_path}" "${host_ipv4dns}" "${command_export_from_docker}"

    echo "Export collection ${collection_name} from host to local"
    current_date=$(date '+%Y%m%d')
    mkdir -p "../outputs/neurochain_db"
    output_path="../outputs/neurochain_db/${collection_name}_${current_date}.json"
    if [ -f "${output_path}" ]; then
        echo "" > ${output_path}
    fi
    scp -i "${pem_path}" -r ubuntu@"${host_ipv4dns}":~/neurochain_db/${collection_name}.json ${output_path}

    echo "Clear collection ${collection_name} from host"
    command_clear_output="rm ~/neurochain_db/${collection_name}.json"
    systemTools_sendSshCommand "${pem_path}" "${host_ipv4dns}" "${command_clear_output}"
)

exportDatabase() {
    pem_path=$1
    ipv4=$2
    database_name=$3

    IFS=', ' read -r -a collections_array <<< $( getCollectionNames "${pem_path}" "${ipv4}" "${database_name}" )
    for (( i = 0; i < ${#collections_array[@]}; ++i )); do
        if [ "${#collections_array[i]}" -gt 1 ]; then

            echo "Exporting collection: ${collections_array[i]}"
            exportCollectionAndDownload "${pem_path}" "${ipv4}" "${database_name}" "${collections_array[i]}"
        fi
    done
}