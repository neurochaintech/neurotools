#!/bin/bash

clear

################################
# Check if user asked for help #
################################

showHelp() {
    printf "
neurochain_controller [-h] [-status]

where:
    -h              show help
    -status         show core and database status (all hosts)
    -start          start core and database (all hosts)
    -restart        restart core and database (all hosts)
    -stop           stop core and database (all hosts)
    -exportDatabase export database from Testnet5-host5
    -resetDatabase  reset database (all hosts)
    
    \n"
}

source "../shared/tools/help_tools.sh"
should_show_help=$( helpTools_userAskedForIt "$@" )
if [ "${should_show_help}" == true ]; then
    showHelp
    exit 0
fi

########
# Main #
########

source "config.sh"
source "../shared/tools/log_tools.sh"
source "../shared/tools/docker_tools.sh"
source "../shared/tools/system_tools.sh"

executeAction() {
    ipv4=$1
    action=$2
    container_id=$3
    
    logTools_print "Container ID: ${container_id} + Action: ${action}"
    dockerTools_executeAction "${PEM_PATH}" "${ipv4}" "${action}" "${container_id}"
    logTools_print "=> DONE"
}

if [ "$1" == "-status" -o "$1" == "-start" -o "$1" == "-stop"  -o "$1" == "-restart" ]; then

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        if [ "$1" == "-status" ]; then
            dockerTools_listContainers "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}"
            printf "\n\n"

        else
            core_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}" )
            database_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}" )
            logTools_print "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}: ${core_id}"
            logTools_print "${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}: ${database_id}"

            action="${1#?}"
            executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${action}" "${core_id}"
            executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${action}" "${database_id}"
            echo ""
        fi

    done

elif [ "$1" == "-exportDatabase" ]; then
    source "./export_database.sh"
    
    host_name="Testnet5-host5"
    host_ipv4dns="ec2-35-181-125-1.eu-west-3.compute.amazonaws.com"

    logTools_print "Exporting database from ${host_name}..."
    exportDatabase "${PEM_PATH}" "${host_ipv4dns}" "${NEUROCHAIN_DATABASE_NAME}"

elif [ "$1" == "-resetDatabase" ]; then

    MONGO_PATH="/home/ubuntu/mongo"

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        core_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}" )
        database_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}" )

        executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "stop" "${core_id}"
        executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "stop" "${database_id}"
    done

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        echo "Removing ${MONGO_PATH}..."
        command="sudo rm -rf ${MONGO_PATH}"
        systemTools_sendSshCommand "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${command}"
    done

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        core_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}" )
        database_id=$( dockerTools_getContainerIdWithImageName "${PEM_PATH}" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_DATABASE_DOCKER_CONTAINER_NAME}" )

        executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "start" "${core_id}"
        executeAction "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "start" "${database_id}"
    done

fi

exit 0