#!/bin/bash

clear

################################
# Check if user asked for help #
################################

showHelp() {
    printf "
neurochain_controller [-h] [-status]

where:
    -h                          show help
    -neurochainNetworkHosts     generate neurochain_network_hosts.json
    -showRemoteJson             show bot.json (all hosts)
    -saveRemoteJson             save bot.json (all hosts)
    -generateJson               generate bot.json
    -uploadJson                 upload bot.json (all hosts)
    
    \n"
}

source "../shared/tools/help_tools.sh"
should_show_help=$( helpTools_userAskedForIt "$@" )
if [ "${should_show_help}" == true ]; then
    showHelp
    exit 0
fi

########
# Main #
########

source "config.sh"
source "../shared/tools/log_tools.sh"
source "../shared/tools/docker_tools.sh"
source "../shared/tools/system_tools.sh"
source "../shared/tools/neurochain_bot_tools.sh"

NEUROCHAIN_JSON_PATH="../outputs/neurochain_network_hosts.json"
BOT_JSON_BEGIN="../shared/templates/bot_json_begin.txt"
BOT_JSON_END="../shared/templates/bot_json_end.txt"
BOT_CONFIGURATION_FILE="bot.json"
BOT_CONFIGURATION_PATH="~/conf/${BOT_CONFIGURATION_FILE}"

if [ "$1" == "-neurochainNetworkHosts" ]; then

    echo "Generating: ${NEUROCHAIN_JSON_PATH}..."

    if [[ ! -e "${NEUROCHAIN_JSON_PATH}" ]]; then
        mkdir -p "../outputs"
        touch "${NEUROCHAIN_JSON_PATH}"
    fi

    printf '' > ${NEUROCHAIN_JSON_PATH}
    printf "{\n" >> ${NEUROCHAIN_JSON_PATH}
    printf "\t\"hosts\": [\n" >> ${NEUROCHAIN_JSON_PATH}

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        echo "Getting ${NETWORK_HOSTS_NAMES[i_host]} (${NETWORK_HOSTS_IPV4_DNS[i_host]}) data..."
        ip_address=$( systemTools_getSshIpv4 $PEM_PATH ${NETWORK_HOSTS_IPV4_DNS[i_host]} )
        public_key=$( neurochainBotTools_getPublicKey $PEM_PATH ${NETWORK_HOSTS_IPV4_DNS[i_host]} ${ip_address} ${NEUROCHAIN_BOT_API_PORT} )
        
        echo "ip_address: ${ip_address}"
        echo "public_key: ${public_key}"
        
        printf "\t\t{\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t\t\"ipv4\": \"${ip_address}\",\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t\t\"name\": \"${NETWORK_HOSTS_NAMES[i_host]}\",\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t\t\"port\": \"3000\",\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t\t\"urlRadix\": \"\",\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t\t\"publicKey\": \"${public_key}\"\n" >> ${NEUROCHAIN_JSON_PATH}
        printf "\t\t}" >> ${NEUROCHAIN_JSON_PATH}

        if [[ "$i_host" -lt "${#NETWORK_HOSTS_IPV4_DNS[@]} -1" ]]; then
            printf ",\n" >> ${NEUROCHAIN_JSON_PATH}
        else
            printf "\n" >> ${NEUROCHAIN_JSON_PATH}
        fi
    done

    printf "\t]\n" >> ${NEUROCHAIN_JSON_PATH}
    printf "}" >> ${NEUROCHAIN_JSON_PATH}

    echo "Done"

elif [ "$1" == "-showRemoteJson" ]; then

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        command="cat ${BOT_CONFIGURATION_PATH}"
        printf "$( systemTools_sendSshCommand "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${command}" )\n\n\n\n"
    done

elif [ "$1" == "-saveRemoteJson" ]; then

    current_date=$(date '+%Y%m%d')
    extension="${BOT_CONFIGURATION_FILE##*.}"
    filename="${BOT_CONFIGURATION_FILE%.*}"

    save_file="${filename}_${current_date}.${extension}"
    save_path="${BOT_CONFIGURATION_PATH/${BOT_CONFIGURATION_FILE}/${save_file}}"

    for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
        logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
        logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

        command="cp ${BOT_CONFIGURATION_PATH} ${save_path}"
        $( systemTools_sendSshCommand "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${command}" )
        
        printf "Saved: ${save_path}\n\n"
    done

elif [ "$1" == "-generateJson" ]; then

    if [ ! -f ${NEUROCHAIN_JSON_PATH} ]; then
        echo "File not found: ${NEUROCHAIN_JSON_PATH}, please generate with -neurochainNetworkHosts"
    else
        output_path="../outputs/${BOT_CONFIGURATION_FILE}"
        echo "" > ${output_path}
        cat ${BOT_JSON_BEGIN} >> ${output_path}

        json_content=`cat ${NEUROCHAIN_JSON_PATH}`
        hosts_ipv4s=( $( jq -r '.hosts[].ipv4' <<< ${json_content} ) )
        hosts_public_keys=( $( jq -r '.hosts[].publicKey' <<< ${json_content} ) )
        for (( iHost = 0; iHost < ${#hosts_ipv4s[@]}; ++iHost )); do
            ipv4=${hosts_ipv4s[iHost]}
            public_key=${hosts_public_keys[iHost]}

            printf '\t\t\t\t{\n'  >> ${output_path}
            printf '\t\t\t\t\t"endpoint": "'${ipv4}'",\n' >> ${output_path}
            printf '\t\t\t\t\t"keyPub": {\n' >> ${output_path}
            printf '\t\t\t\t\t\t"rawData": "'${public_key}'"\n' >> ${output_path}
            printf '\t\t\t\t\t},\n' >> ${output_path}
            printf '\t\t\t\t\t"port": "1337"\n' >> ${output_path}
            printf "\t\t\t\t}" >> ${output_path}

            if [[ "$iHost" -lt "${#hosts_ipv4s[@]} -1" ]]; then
                printf ',\n' >> ${output_path}
            else
                printf '\n' >> ${output_path}
            fi
        done

        cat ${BOT_JSON_END} >> ${output_path}

        echo "Generated: ${output_path}"
    fi

elif [ "$1" == "-uploadJson" ]; then

    file_to_upload_path="../outputs/${BOT_CONFIGURATION_FILE}"
    if [ ! -f ${file_to_upload_path} ]; then
        echo "File not found: ${file_to_upload_path}, please generateJson"
    else

        for (( i_host = 0; i_host < ${#NETWORK_HOSTS_NAMES[@]}; ++i_host )); do
            logTools_print "Name: ${NETWORK_HOSTS_NAMES[i_host]}"
            logTools_print "IPv4 DNS: ${NETWORK_HOSTS_IPV4_DNS[i_host]}"

            core_id=$( dockerTools_getContainerIdWithImageName "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${NEUROCHAIN_CORE_DOCKER_CONTAINER_NAME}" )
            if [[ ! -z "${core_id}" ]]
            then
                dockerTools_executeAction "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "stop" "${core_id}"
                
                echo "Sending file: ${BOT_CONFIGURATION_FILE}"
                systemTools_sendFilesWithSsh "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "${file_to_upload_path}" "${BOT_CONFIGURATION_PATH}"

                dockerTools_executeAction "$PEM_PATH" "${NETWORK_HOSTS_IPV4_DNS[i_host]}" "start" "${core_id}"
            fi
        done
        
    fi
fi

exit 0