# Vault Password Management

## Consul

### Install 

Consul is a highly scalable and distributed service discovery and configuration system. 
Consul Storage can be used as a back-end to Vault to ensure the software is highly available and fault-tolerant.

```
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install consul

# Check: show help
consul
```

### Configure

```
sudo nano /etc/systemd/system/consul.service

# Get ip address to adapt -bind:
ifconfig
... inet 192.168.1.18

# Add the following content to the consul.service file:
[Unit]
Description=Consul
Documentation=https://www.consul.io/
 
[Service]
ExecStart=/usr/bin/consul agent -server -ui -data-dir=/tmp/consul -bootstrap-expect=1 -node=vault -bind=192.168.1.18 -config-dir=/etc/consul.d/
ExecReload=/bin/kill -HUP $MAINPID
LimitNOFILE=65536
 
[Install]
WantedBy=multi-user.target

 
```

```
sudo mkdir /etc/consul.d
sudo nano /etc/consul.d/ui.json

# Add the following content to the consul.service file:
{
    "addresses": {
        "http": "0.0.0.0"
    }
}
```

```
# Reload
sudo systemctl daemon-reload
sudo systemctl start consul
sudo systemctl enable consul

# Verify 
consul members
Node   Address            Status  Type    Build   Protocol  DC   Partition  Segment
vault  192.168.1.18:8301  alive   server  1.13.2  2         dc1  default    <all>
```

## Vault

### Install

```
sudo apt update && sudo apt install gpg
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null

# Verify key
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
-------------------------------------------------
pub   rsa4096 2020-05-07 [SC]
      E8A0 32E0 94D8 EB4E A189  D270 DA41 8C88 A321 9F7B
uid           [ unknown] HashiCorp Security (HashiCorp Package Signing) <security+packaging@hashicorp.com>
sub   rsa4096 2020-05-07 [E]

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vault

# Check: show help
vault
```

### Configure

```
sudo mkdir /etc/vault
sudo nano /etc/vault/config.hcl

# Add the following content to the consul.service file:
storage "consul" {
    address = "127.0.0.1:8500"
    path    = "vault/"
}
 
listener "tcp" {
    address     = "192.168.1.18:8200"
    tls_disable = 1
}
 
ui = true
```

```
sudo nano /etc/systemd/system/vault.service
# Add the following content to the consul.service file:
[Unit]
Description=Vault
Documentation=https://www.vault.io/
 
[Service]
ExecStart=/usr/bin/vault server -config=/etc/vault/config.hcl
ExecReload=/bin/kill -HUP $MAINPID
LimitNOFILE=65536
 
[Install]
WantedBy=multi-user.target
```

### Launch

```
# Reload
sudo systemctl daemon-reload
sudo systemctl start vault
sudo systemctl enable vault

# To enable the CLI to connect to Vault service
export VAULT_ADDR=http://192.168.1.18:8200

vault operator init
# Show 5 unseal keys
Unseal Key 1: 2ixCEqYl8rxmxQGVDQWfZ9ar430/OvXzS5GtxDv+N2YU
Unseal Key 2: fP8dvLjlkWEF6pGxCXlUpPdIvEvtO7uBaZOgO6CUnDK9
Unseal Key 3: N1zBJeP4C0LKD9BN/Vig4Wvcc7Dh330FWFbSuVsyg53z
Unseal Key 4: ZBLH6QPQSmEz9E9ZzFT7Oaa73NzUgqZpNYkkM4cyb6/0
Unseal Key 5: RPurfy2nrEx0Qw06F/CO18i+aD3joVddDg0gIYN14PG0

Initial Root Token: hvs.qT5ejEyb1Xgue5AvHQRtQAwa
```

```
vault server -dev

...
You may need to set the following environment variable:

    $ export VAULT_ADDR='http://127.0.0.1:8200'

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

Unseal Key: vF0LnTK347aoATbV5AffEjYQIEizjXdcnEVfXV2TSyM=
Root Token: hvs.2WEHFj2fl8zGCXc5JxIXtifS
```

In another terminal:
```
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN="hvs.2WEHFj2fl8zGCXc5JxIXtifS"

vault status
```

```
vault operator unseal
Unseal Key (will be hidden): # Use keys from "vault operator init"
Key                Value
---                -----
Seal Type          shamir
Initialized        true
Sealed             true
Total Shares       5
Threshold          3
Unseal Progress    1/3
Unseal Nonce       4cf835f5-4997-fd9a-6e10-f19fc4be9fc3
Version            1.11.4
Build Date         2022-09-23T06:01:14Z
Storage Type       consul
HA Enabled         true
```

## Manage secret keys

### Create / Update

Write a key-value secret to the path "hello", with a key named "foo" and value of "world".
This command creates a new version of the secrets and replaces any pre-existing data at the path if any:
```
vault kv put -mount=secret hello foo=world

== Secret Path ==
secret/data/hello

======= Metadata =======
Key                Value
---                -----
created_time       2022-10-12T08:21:12.656154383Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1
```

### Read

```
vault kv get -mount=secret hello

== Secret Path ==
secret/data/hello

======= Metadata =======
Key                Value
---                -----
created_time       2022-10-12T08:21:12.656154383Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

=== Data ===
Key    Value
---    -----
foo    world
```

Vault returns the latest version (in this case version 2) of the secrets at secret/hello.

To print only the value of a given field, use the -field=<key_name> flag.
```
vault kv get -mount=secret -field=foo hello

world
```

Optional JSON output is very useful for scripts. For example, you can use the jq tool to extract the value of the excited secret.
```
vault kv get -mount=secret -format=json hello

{
    "request_id": "3ad3ad89-1a78-c709-d5b4-9496ab70a427",
    "lease_id": "",
    "lease_duration": 0,
    "renewable": false,
    "data": {
        "data": {
            "foo": "world"
        },
            "metadata": {
            "created_time": "2022-10-12T08:21:12.656154383Z",
            "custom_metadata": null,
            "deletion_time": "",
            "destroyed": false,
            "version": 1
        }
    },
    "warnings": null
}
```

### Delete

```
vault kv delete -mount=secret hello

Success! Data deleted (if it existed) at: secret/data/hello
```